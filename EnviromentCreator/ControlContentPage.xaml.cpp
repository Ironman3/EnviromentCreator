﻿//
// ControlContentPage.xaml.cpp
// Implementation of the ControlContentPage class
//

#include "pch.h"
#include "ControlContentPage.xaml.h"

using namespace EnviromentCreator;

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::Storage::Streams;
using namespace Windows::System::Threading;
using namespace concurrency;

using namespace std;

using namespace Engine;
using namespace Engine::Stream;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

ControlContentPage::ControlContentPage()
{
	InitializeComponent();

	shaderEffectComboBox->Items->Append(L"Diffuse");
	shaderEffectComboBox->Items->Append(L"Specular");
	shaderEffectComboBox->SelectedIndex = 0;
}

void EnviromentCreator::ControlContentPage::modelContentLoadButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	FileOpenPicker^ openPicker = ref new FileOpenPicker();
	openPicker->ViewMode = PickerViewMode::Thumbnail;
	openPicker->SuggestedStartLocation = PickerLocationId::PicturesLibrary;
	openPicker->FileTypeFilter->Append(".md3");

	create_task(openPicker->PickSingleFileAsync()).then([this](StorageFile^ file)
	{
		if (file)
		{
			create_task(FileIO::ReadBufferAsync(file)).then([=](IBuffer^ fileBuffer)
			{
				FileData ^dataBuffer = ref new FileData(fileBuffer->Length);
				DataReader::FromBuffer(fileBuffer)->ReadBytes(
					ArrayReference<byte>(dataBuffer->Data, fileBuffer->Length)
				);

				DirectXPage::GetEngine()->GetContent().BuildModelMeshStream(
					FindNextModelContent(file->DisplayName),
					dataBuffer
				);
			}).then([this]
			{
				ReloadModelContentList();
			});
		}
	});
}

void EnviromentCreator::ControlContentPage::modelContentExcludeButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto enginePtr = DirectXPage::GetEngine();

	enginePtr->GetContent().DeleteModelMeshStreamByCursor();
	enginePtr->GetGraphics().DeleteOutputByContent();
	ReloadModelContentList();
}

void EnviromentCreator::ControlContentPage::shaderEffectComboBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)
{
	auto devicePtr = DirectXPage::GetEngine();
	if (!shaderEffectComboBox->SelectedItem || !devicePtr) return;

	if (shaderEffectComboBox->SelectedItem->ToString() == L"Diffuse")
	{
		devicePtr->GetGraphics().UseResource(Graphics::RESOURCE_TYPE::VertexShader, L"vs_light_diffuse");
		devicePtr->GetGraphics().UseResource(Graphics::RESOURCE_TYPE::PixelShader, L"ps_light_diffuse");
	}
	else if (shaderEffectComboBox->SelectedItem->ToString() == L"Specular")
	{
		devicePtr->GetGraphics().UseResource(Graphics::RESOURCE_TYPE::VertexShader, L"vs_light_specular");
		devicePtr->GetGraphics().UseResource(Graphics::RESOURCE_TYPE::PixelShader, L"ps_light_specular");
	}
}

void EnviromentCreator::ControlContentPage::modelContentListBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)
{
	auto selectedItem = modelContentListBox->SelectedItem;
	if (selectedItem)
	{
		DirectXPage::GetEngine()->GetContent().SetModelMeshStreamCursor(
			selectedItem->ToString()->Data()
		);
	}
}

std::wstring ControlContentPage::FindNextModelContent(
	_In_ String ^catalogName
)
{
	int nextIndex = 0;
	auto enginePtr = DirectXPage::GetEngine();
	auto vaildName = std::wstring(catalogName->Data()) + L'-';

	while (enginePtr->GetContent().ModelMeshStreamIsExsist(vaildName + nextIndex.ToString()->Data()))
	{
		++nextIndex;
	}

	return vaildName + nextIndex.ToString()->Data();
}

void ControlContentPage::ReloadModelContentList()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (enginePtr)
	{
		vector<wstring> modelContentQuery;
		enginePtr->GetContent().QueryModelMeshStream(modelContentQuery);

		IVector<String^> ^tmpList = ref new Vector<String^>();
		for (auto &modelContent : modelContentQuery)
			tmpList->Append(ref new String(modelContent.data()));

		modelContentListBox->ItemsSource = tmpList;
		modelContentListBox->SelectedIndex = modelContentListBox->Items->Size - 1;
	}
}

void EnviromentCreator::ControlContentPage::shaderEffectComboBox_KeyDown(Platform::Object^ sender, Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e)
{
	if (e->Key != Windows::System::VirtualKey::Up || e->Key != Windows::System::VirtualKey::Down)
		e->Handled = true;
}
