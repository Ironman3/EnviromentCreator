﻿//
// ControlContentPage.xaml.h
// Declaration of the ControlContentPage class
//

#pragma once

#include "ControlContentPage.g.h"
#include "pch.h"

namespace EnviromentCreator
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ControlContentPage sealed
	{
	public:
		ControlContentPage();
	private:
		void modelContentLoadButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void modelContentExcludeButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		void shaderEffectComboBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
		void editorToolsCheckBox_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void modelContentListBox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);

	private:
		void ReloadModelContentList();
	
		std::wstring FindNextModelContent(
			_In_ Platform::String^ catalogName
		);
		void shaderEffectComboBox_KeyDown(Platform::Object^ sender, Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e);
	};
}
