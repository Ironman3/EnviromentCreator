﻿//
// ControlCameraPage.xaml.h
// Declaration of the ControlCameraPage class
//

#pragma once

#include "ControlCameraAndLightPage.g.h"
#include "pch.h"

namespace EnviromentCreator
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ControlCameraAndLightPage sealed
	{
	public:
		ControlCameraAndLightPage();

	public:
		property Platform::String^ CameraPositionX
		{
			Platform::String^ get() { return GetCameraPositionX().ToString(); }
			void set(Platform::String^ value) { SetCameraPositionX((float)_wtof(value->Data())); }
		}
		property Platform::String^ CameraPositionY
		{
			Platform::String^ get() { return GetCameraPositionY().ToString(); }
			void set(Platform::String^ value) { SetCameraPositionY((float)_wtof(value->Data())); }
		}
		property Platform::String^ CameraPositionZ
		{
			Platform::String^ get() { return GetCameraPositionZ().ToString(); }
			void set(Platform::String^ value) { SetCameraPositionZ((float)_wtof(value->Data())); }
		}
		void SetCameraPositionX(
			_In_ float value
		);
		void SetCameraPositionY(
			_In_ float value
		);
		void SetCameraPositionZ(
			_In_ float value
		);
		float GetCameraPositionX();
		float GetCameraPositionY();
		float GetCameraPositionZ();

	public:
		property Platform::String^ LightValueX
		{
			Platform::String^ get() { return GetLightValueX().ToString(); }
			void set(Platform::String^ value) { SetLightValueX((float)_wtof(value->Data())); }
		}
		property Platform::String^ LightValueY
		{
			Platform::String^ get() { return GetLightValueY().ToString(); }
			void set(Platform::String^ value) { SetLightValueY((float)_wtof(value->Data())); }
		}
		property Platform::String^ LightValueZ
		{
			Platform::String^ get() { return GetLightValueZ().ToString(); }
			void set(Platform::String^ value) { SetLightValueZ((float)_wtof(value->Data())); }
		}
		void SetLightValueX(
			_In_ float value
		);
		void SetLightValueY(
			_In_ float value
		);
		void SetLightValueZ(
			_In_ float value
		);
		float GetLightValueX();
		float GetLightValueY();
		float GetLightValueZ();

		property Platform::String^ LightColorR
		{
			Platform::String^ get() { return GetLightColorR().ToString(); }
			void set(Platform::String^ value) { SetLightColorR((float)_wtof(value->Data())); }
		}
		property Platform::String^ LightColorG
		{
			Platform::String^ get() { return GetLightColorG().ToString(); }
			void set(Platform::String^ value) { SetLightColorG((float)_wtof(value->Data())); }
		}
		property Platform::String^ LightColorB
		{
			Platform::String^ get() { return GetLightColorB().ToString(); }
			void set(Platform::String^ value) { SetLightColorB((float)_wtof(value->Data())); }
		}
		void SetLightColorR(
			_In_ float value
		);
		void SetLightColorG(
			_In_ float value
		);
		void SetLightColorB(
			_In_ float value
		);
		float GetLightColorR();
		float GetLightColorG();
		float GetLightColorB();

	private:
		void cameraPositionX_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void cameraPositionX_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void cameraPositionY_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void cameraPositionY_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void cameraPositionZ_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void cameraPositionZ_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);

	private:
		void lightResourceListbox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
		void lightResourceCreateButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void lightResourceRemoveButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void lightPositionRadioButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void lightDirectionRadioButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		void lightValueX_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void lightValueX_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void lightValueY_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void lightValueY_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void lightValueZ_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void lightValueZ_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);

		void lightColorR_Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e);
		void lightColorG_Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e);
		void lightColorB_Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e);

	private:
		void ReloadCameraResourceList();
		void ReloadLightResourceList();

		std::wstring				GetUniqueCameraResourceName();
		std::wstring				GetUniqueLightResourceName();

		Engine::Camera::BaseCamera*	GetCameraSelected();
		Engine::Light::BaseLight*	GetLightSelected();

	private:
		Windows::Foundation::IAsyncAction^	m_idleLoopWorker;
		DirectX::XMFLOAT3					m_cached_cameraPosition;
		DirectX::XMFLOAT3					m_cached_lightValue;
		DirectX::XMFLOAT4					m_cached_lightColor;
		int									m_cached_lightValueId;
	};
}
