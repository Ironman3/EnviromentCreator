﻿//
// ControlCameraPage.xaml.cpp
// Implementation of the ControlCameraPage class
//

#include "pch.h"
#include "ControlCameraAndLightPage.xaml.h"

using namespace EnviromentCreator;

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Core;
using namespace Windows::System::Threading;

using namespace std;

using namespace Engine;
using namespace Engine::Camera;
using namespace Engine::Light;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

ControlCameraAndLightPage::ControlCameraAndLightPage()
{
	InitializeComponent();

	lightResourceTypesComboBox->Items->Append(L"Sun");
	lightResourceTypesComboBox->Items->Append(L"Point");
	//lightResourceTypesComboBox->Items->Append(L"Flash");
	lightResourceTypesComboBox->SelectedIndex = 0;

	lightPositionRadioButton->IsEnabled = false;
	lightDirectionRadioButton->IsEnabled = false;

	lightValueX_TextBox->IsEnabled = false;
	lightValueY_TextBox->IsEnabled = false;
	lightValueZ_TextBox->IsEnabled = false;

	lightColorR_Slider->IsEnabled = false;
	lightColorG_Slider->IsEnabled = false;
	lightColorB_Slider->IsEnabled = false;

	auto workItemHandlerForIdle = ref new WorkItemHandler([this](IAsyncAction ^action)
	{
		while (action->Status == AsyncStatus::Started)
		{
			if (m_cached_cameraPosition.x != GetCameraPositionX())
			{
				cameraPositionX_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([=]() {
					m_cached_cameraPosition.x = GetCameraPositionX();
					cameraPositionX_TextBox->Text = CameraPositionX;
				}));
			}
			if (m_cached_cameraPosition.y != GetCameraPositionY())
			{
				cameraPositionY_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([=]() {
					m_cached_cameraPosition.y = GetCameraPositionY();
					cameraPositionY_TextBox->Text = CameraPositionY;
				}));
			}
			if (m_cached_cameraPosition.z != GetCameraPositionZ())
			{
				cameraPositionZ_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([=]() {
					m_cached_cameraPosition.z = GetCameraPositionZ();
					cameraPositionZ_TextBox->Text = CameraPositionZ;
				}));
			}

			if (m_cached_lightValue.x != GetLightValueX())
			{
				lightValueX_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([=]() {
					m_cached_lightValue.x = GetLightValueX();
					lightValueX_TextBox->Text = LightValueX;
				}));
			}
			if (m_cached_lightValue.y != GetLightValueY())
			{
				lightValueY_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([=]() {
					m_cached_lightValue.y = GetLightValueY();
					lightValueY_TextBox->Text = LightValueY;
				}));
			}
			if (m_cached_lightValue.z != GetLightValueZ())
			{
				lightValueZ_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([=]() {
					m_cached_lightValue.z = GetLightValueZ();
					lightValueZ_TextBox->Text = LightValueZ;
				}));
			}
		}
	});
	m_idleLoopWorker = ThreadPool::RunAsync(workItemHandlerForIdle, WorkItemPriority::Low, WorkItemOptions::TimeSliced);
}

float ControlCameraAndLightPage::GetCameraPositionX()
{
	auto selectedCamera = GetCameraSelected();
	return !selectedCamera ? 0.0f : selectedCamera->GetEye().x;
}

float ControlCameraAndLightPage::GetCameraPositionY()
{
	auto selectedCamera = GetCameraSelected();
	return !selectedCamera ? 0.0f : selectedCamera->GetEye().y;
}

float ControlCameraAndLightPage::GetCameraPositionZ()
{
	auto selectedCamera = GetCameraSelected();
	return !selectedCamera ? 0.0f : selectedCamera->GetEye().z;
}

void ControlCameraAndLightPage::SetCameraPositionX(float value)
{
	auto selectedCamera = GetCameraSelected();
	if (selectedCamera)
	{
		m_cached_cameraPosition.x = value;
		selectedCamera->SetEyePosition(m_cached_cameraPosition);
	}
}

void ControlCameraAndLightPage::SetCameraPositionY(float value)
{
	auto selectedCamera = GetCameraSelected();
	if (selectedCamera)
	{
		m_cached_cameraPosition.y = value;
		selectedCamera->SetEyePosition(m_cached_cameraPosition);
	}
}

void ControlCameraAndLightPage::SetCameraPositionZ(float value)
{
	auto selectedCamera = GetCameraSelected();
	if (selectedCamera)
	{
		m_cached_cameraPosition.z = value;
		selectedCamera->SetEyePosition(m_cached_cameraPosition);
	}
}

//==================================

void ControlCameraAndLightPage::SetLightValueX(
	_In_ float value
)
{	
	auto selectedLight = GetLightSelected();
	if (selectedLight)
	{
		m_cached_lightValue.x = value;
		if (lightPositionRadioButton->IsChecked->Value)
		{
			selectedLight->SetPosition(
				DirectX::XMFLOAT3(
					m_cached_lightValue.x, 
					m_cached_lightValue.y,
					m_cached_lightValue.z
				)
			);
		}
		else if (lightDirectionRadioButton->IsChecked->Value)
		{
			selectedLight->SetDirection(
				DirectX::XMFLOAT3(
					m_cached_lightValue.x,
					m_cached_lightValue.y,
					m_cached_lightValue.z
				)
			);
		}
	}
}

void ControlCameraAndLightPage::SetLightValueY(
	_In_ float value
)
{
	auto selectedLight = GetLightSelected();
	if (selectedLight)
	{
		m_cached_lightValue.y = value;
		if (lightPositionRadioButton->IsChecked->Value)
		{
			selectedLight->SetPosition(
				DirectX::XMFLOAT3(
					m_cached_lightValue.x,
					m_cached_lightValue.y,
					m_cached_lightValue.z
				)
			);
		}
		else if (lightDirectionRadioButton->IsChecked->Value)
		{
			selectedLight->SetDirection(
				DirectX::XMFLOAT3(
					m_cached_lightValue.x,
					m_cached_lightValue.y,
					m_cached_lightValue.z
				)
			);
		}
	}
}

void ControlCameraAndLightPage::SetLightValueZ(
	_In_ float value
)
{
	auto selectedLight = GetLightSelected();
	if (selectedLight)
	{
		m_cached_lightValue.z = value;
		if (lightPositionRadioButton->IsChecked->Value)
		{
			selectedLight->SetPosition(
				DirectX::XMFLOAT3(
					m_cached_lightValue.x,
					m_cached_lightValue.y,
					m_cached_lightValue.z
				)
			);
		}
		else if (lightDirectionRadioButton->IsChecked->Value)
		{
			selectedLight->SetDirection(
				DirectX::XMFLOAT3(
					m_cached_lightValue.x,
					m_cached_lightValue.y,
					m_cached_lightValue.z
				)
			);
		}
	}
}

float ControlCameraAndLightPage::GetLightValueX()
{
	auto selectedLight = GetLightSelected();
	switch (m_cached_lightValueId)
	{
	case 0:
		return !selectedLight ? 0.0f : selectedLight->GetPosition().x;
	case 1:
		return !selectedLight ? 0.0f : selectedLight->GetDirection().x;
	default:
		return 0.0f;
	}
}

float ControlCameraAndLightPage::GetLightValueY()
{
	auto selectedLight = GetLightSelected();
	switch (m_cached_lightValueId)
	{
	case 0:
		return !selectedLight ? 0.0f : selectedLight->GetPosition().y;
	case 1:
		return !selectedLight ? 0.0f : selectedLight->GetDirection().y;
	default:
		return 0.0f;
	}
}

float ControlCameraAndLightPage::GetLightValueZ()
{
	auto selectedLight = GetLightSelected();
	switch (m_cached_lightValueId)
	{
	case 0:
		return !selectedLight ? 0.0f : selectedLight->GetPosition().z;
	case 1:
		return !selectedLight ? 0.0f : selectedLight->GetDirection().z;
	default:
		return 0.0f;
	}
}


void ControlCameraAndLightPage::SetLightColorR(
	_In_ float value
)
{
	auto selectedLight = GetLightSelected();
	if (selectedLight)
	{
		auto rgbaTmp = selectedLight->GetColor_RGBA();
		rgbaTmp.x = value;
		selectedLight->SetColor(rgbaTmp);
	}
}

void ControlCameraAndLightPage::SetLightColorG(
	_In_ float value
)
{
	auto selectedLight = GetLightSelected();
	if (selectedLight)
	{
		auto rgbaTmp = selectedLight->GetColor_RGBA();
		rgbaTmp.y = value;
		selectedLight->SetColor(rgbaTmp);
	}
}

void ControlCameraAndLightPage::SetLightColorB(
	_In_ float value
)
{
	auto selectedLight = GetLightSelected();
	if (selectedLight)
	{
		auto rgbaTmp = selectedLight->GetColor_RGBA();
		rgbaTmp.z = value;
		selectedLight->SetColor(rgbaTmp);
	}
}

float ControlCameraAndLightPage::GetLightColorR()
{
	auto selectedLight = GetLightSelected();
	return !selectedLight ? 0.0f : selectedLight->GetColor_RGB().x;
}

float ControlCameraAndLightPage::GetLightColorG()
{
	auto selectedLight = GetLightSelected();
	return !selectedLight ? 0.0f : selectedLight->GetColor_RGB().y;
}

float ControlCameraAndLightPage::GetLightColorB()
{
	auto selectedLight = GetLightSelected();
	return !selectedLight ? 0.0f : selectedLight->GetColor_RGB().z;
}

//

void EnviromentCreator::ControlCameraAndLightPage::cameraPositionX_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		cameraPositionX_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		cameraPositionX_TextBox->Text = CameraPositionX;
	}
}

void EnviromentCreator::ControlCameraAndLightPage::cameraPositionX_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	CameraPositionX = cameraPositionX_TextBox->Text;
}

void EnviromentCreator::ControlCameraAndLightPage::cameraPositionY_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		cameraPositionY_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		cameraPositionY_TextBox->Text = CameraPositionY;
	}
}

void EnviromentCreator::ControlCameraAndLightPage::cameraPositionY_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	CameraPositionY = cameraPositionY_TextBox->Text;
}

void EnviromentCreator::ControlCameraAndLightPage::cameraPositionZ_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		cameraPositionZ_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		cameraPositionZ_TextBox->Text = CameraPositionZ;
	}
}

void EnviromentCreator::ControlCameraAndLightPage::cameraPositionZ_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	CameraPositionZ = cameraPositionZ_TextBox->Text;
}

//

void EnviromentCreator::ControlCameraAndLightPage::lightResourceListbox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)
{
	auto enginePtr = DirectXPage::GetEngine();
	if (enginePtr)
	{
	
		if(lightResourceListbox->SelectedItem)
		{
			enginePtr->GetGraphics().SetLightCursor(
				lightResourceListbox->SelectedItem->ToString()->Data()
			);

			lightPositionRadioButton->IsChecked = true;
			lightPositionRadioButton->IsEnabled = true;
			lightDirectionRadioButton->IsEnabled = true;

			lightValueX_TextBox->IsEnabled = true;
			lightValueY_TextBox->IsEnabled = true;
			lightValueZ_TextBox->IsEnabled = true;

			lightColorR_Slider->IsEnabled = true;
			lightColorG_Slider->IsEnabled = true;
			lightColorB_Slider->IsEnabled = true;

			lightValueX_TextBox->Text = LightValueX;
			lightValueY_TextBox->Text = LightValueY;
			lightValueZ_TextBox->Text = LightValueZ;

			lightColorR_Slider->Value = GetLightColorR() * 100.0;
			lightColorG_Slider->Value = GetLightColorG() * 100.0;
			lightColorB_Slider->Value = GetLightColorB() * 100.0;
		}
		else
		{
			lightPositionRadioButton->IsEnabled = false;
			lightDirectionRadioButton->IsEnabled = false;

			lightValueX_TextBox->IsEnabled = false;
			lightValueY_TextBox->IsEnabled = false;
			lightValueZ_TextBox->IsEnabled = false;

			lightColorR_Slider->IsEnabled = false;
			lightColorG_Slider->IsEnabled = false;
			lightColorB_Slider->IsEnabled = false;
		}
	}
}

void EnviromentCreator::ControlCameraAndLightPage::lightResourceCreateButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto enginePtr = DirectXPage::GetEngine();
	auto lightType = lightResourceTypesComboBox->SelectedItem;

	if (enginePtr && lightType && enginePtr->GetGraphics().GetLightNumber() < LIGHT_MAX)
	{
		enginePtr->GetGraphics().AppendLight(
			GetUniqueLightResourceName(),
			Light::LIGHT_TYPES::Sun
		);
		ReloadLightResourceList();
	}
}

void EnviromentCreator::ControlCameraAndLightPage::lightResourceRemoveButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto enginePtr = DirectXPage::GetEngine();
	if (enginePtr)
	{
		enginePtr->GetGraphics().DeleteLightByCursor();
		ReloadLightResourceList();
	}
}

void EnviromentCreator::ControlCameraAndLightPage::lightPositionRadioButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (lightPositionRadioButton->IsChecked->Value)
	{
		m_cached_lightValueId = 0;
	}
	else if (lightDirectionRadioButton->IsChecked->Value)
	{
		m_cached_lightValueId = 1;
	}

	lightValueX_TextBox->Text = LightValueX;
	lightValueY_TextBox->Text = LightValueY;
	lightValueZ_TextBox->Text = LightValueZ;
}

void EnviromentCreator::ControlCameraAndLightPage::lightDirectionRadioButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (lightPositionRadioButton->IsChecked->Value)
	{
		m_cached_lightValueId = 0;
	}
	else if (lightDirectionRadioButton->IsChecked->Value)
	{
		m_cached_lightValueId = 1;
	}

	lightValueX_TextBox->Text = LightValueX;
	lightValueY_TextBox->Text = LightValueY;
	lightValueZ_TextBox->Text = LightValueZ;
}



void EnviromentCreator::ControlCameraAndLightPage::lightValueX_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		lightValueX_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		lightValueX_TextBox->Text = LightValueX;
	}
}

void EnviromentCreator::ControlCameraAndLightPage::lightValueX_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	LightValueX = lightValueX_TextBox->Text;
}

void EnviromentCreator::ControlCameraAndLightPage::lightValueY_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		lightValueY_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		lightValueY_TextBox->Text = LightValueY;
	}
}

void EnviromentCreator::ControlCameraAndLightPage::lightValueY_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	LightValueY = lightValueY_TextBox->Text;
}

void EnviromentCreator::ControlCameraAndLightPage::lightValueZ_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		lightValueZ_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		lightValueZ_TextBox->Text = LightValueZ;
	}
}

void EnviromentCreator::ControlCameraAndLightPage::lightValueZ_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	LightValueZ = lightValueZ_TextBox->Text;
}

void EnviromentCreator::ControlCameraAndLightPage::lightColorR_Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e)
{
	double invRange = 1.0 / 100.0;
	SetLightColorR(
		lightColorR_Slider->Value * invRange
	);
}

void EnviromentCreator::ControlCameraAndLightPage::lightColorG_Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e)
{
	double invRange = 1.0 / 100.0;
	SetLightColorG(
		lightColorG_Slider->Value * invRange
	);
}

void EnviromentCreator::ControlCameraAndLightPage::lightColorB_Slider_ValueChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::Primitives::RangeBaseValueChangedEventArgs^ e)
{
	double invRange = 1.0 / 100.0;
	SetLightColorB(
		lightColorB_Slider->Value * invRange
		);
}

//

void ControlCameraAndLightPage::ReloadCameraResourceList()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (enginePtr)
	{
		vector<wstring> cameraQuery;
		enginePtr->GetGraphics().QueryCamera(cameraQuery);

		IVector<String^> ^tmpList = ref new Vector<String^>();
		for (auto &camera : cameraQuery)
			tmpList->Append(ref new String(camera.data()));

		cameraResourceListbox->ItemsSource = tmpList;
		cameraResourceListbox->SelectedIndex = cameraResourceListbox->Items->Size - 1;
	}
}

void ControlCameraAndLightPage::ReloadLightResourceList()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (enginePtr)
	{
		vector<wstring> lightQuery;
		enginePtr->GetGraphics().QueryLight(lightQuery);

		IVector<String^> ^tmpList = ref new Vector<String^>();
		for (auto &light : lightQuery)
			tmpList->Append(ref new String(light.data()));

		lightResourceListbox->ItemsSource = tmpList;
		lightResourceListbox->SelectedIndex = lightResourceListbox->Items->Size - 1;
	}
}

wstring ControlCameraAndLightPage::GetUniqueCameraResourceName()
{
	int nextIndex = 0;
	auto enginePtr = DirectXPage::GetEngine();
	auto uniqueName = enginePtr->GetGraphics().GetCameraCursor() + L'-';

	while (enginePtr->GetGraphics().CameraIsExsist(uniqueName + nextIndex.ToString()->Data()))
	{
		++nextIndex;
	}

	return uniqueName + nextIndex.ToString()->Data();
}

wstring ControlCameraAndLightPage::GetUniqueLightResourceName()
{
	int nextIndex = 0;
	auto enginePtr = DirectXPage::GetEngine();
	auto selectedItem = lightResourceTypesComboBox->SelectedItem;

	if (enginePtr && selectedItem)
	{
		auto uniqueName = selectedItem->ToString() + L'-';

		while (enginePtr->GetGraphics().LightIsExsist((uniqueName + nextIndex.ToString())->Data()))
		{
			++nextIndex;
		}

		return (uniqueName + nextIndex)->Data();
	}
	else
	{
		ThrowIfFailed(E_FAIL);
	}
}

BaseCamera* ControlCameraAndLightPage::GetCameraSelected()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (!enginePtr)
	{
		return nullptr;
	}
	else
	{
		return enginePtr->GetGraphics().GetCameraByCursor();
	}
}

BaseLight* ControlCameraAndLightPage::GetLightSelected()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (!enginePtr)
	{
		return nullptr;
	}
	else
	{
		return enginePtr->GetGraphics().GetLightByCursor();
	}
}



