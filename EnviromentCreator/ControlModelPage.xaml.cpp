﻿//
// ControlModelPage.xaml.cpp
// Implementation of the ControlModelPage class
//

#include "pch.h"
#include "ControlModelPage.xaml.h"

using namespace EnviromentCreator;

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Core;
using namespace Windows::System::Threading;

using namespace std;

using namespace Engine;
using namespace Engine::Stream;
using namespace Engine::Resource;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236
//
//void ControlModelPage::employeeChanged(Object^ sender, Windows::UI::Xaml::Data::PropertyChangedEventArgs^ e)
//{
//}

ControlModelPage::ControlModelPage() 
{
	InitializeComponent();

	modelPositionRadioButton->IsEnabled = false;
	modelRotationRadioButton->IsEnabled = false;
	modelScaleRadioButton->IsEnabled = false;

	modelValueX_TextBox->IsEnabled = false;
	modelValueY_TextBox->IsEnabled = false;
	modelValueZ_TextBox->IsEnabled = false;

	auto workItemHandlerForIdle = ref new WorkItemHandler([this](IAsyncAction ^action)
	{
		while (action->Status == AsyncStatus::Started)
		{
			if (m_cached_modelContent != GetSelectedModelContent())
			{
				modelResourceListbox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([this]() {
					ReloadModelResourceList();
					m_cached_modelContent = GetSelectedModelContent();
				}));
			}

			if (m_cached_modelResourceValue.x != GetModelValueX())
			{
				modelValueX_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([this]() {
					m_cached_modelResourceValue.x = GetModelValueX();
					modelValueX_TextBox->Text = ModelValueX;
				}));
			}
			if (m_cached_modelResourceValue.y != GetModelValueY())
			{
				modelValueY_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([this]() {
					m_cached_modelResourceValue.y = GetModelValueY();
					modelValueY_TextBox->Text = ModelValueY;
				}));
			}
			if (m_cached_modelResourceValue.z != GetModelValueZ())
			{
				modelValueZ_TextBox->Dispatcher->RunAsync(CoreDispatcherPriority::Low, ref new DispatchedHandler([this]() {
					m_cached_modelResourceValue.z = GetModelValueZ();
					modelValueZ_TextBox->Text = ModelValueZ;
				}));
			}
		}
	});
	m_idleLoopWorker = ThreadPool::RunAsync(workItemHandlerForIdle, WorkItemPriority::Low, WorkItemOptions::TimeSliced);
}

//property
void ControlModelPage::SetModelValueX(float value)
{
	auto selectedModel = GetSelectedModelResource();
	if (selectedModel)
	{
		m_cached_modelResourceValue.x = value;
		if (modelPositionRadioButton->IsChecked->Value)
		{
			selectedModel->position.x = m_cached_modelResourceValue.x;
		}
		else if (modelRotationRadioButton->IsChecked->Value)
		{
			selectedModel->rotation.x = m_cached_modelResourceValue.x;
		}
		else if (modelScaleRadioButton->IsChecked->Value)
		{
			selectedModel->scale.x = m_cached_modelResourceValue.x;
		}
	}
}

void ControlModelPage::SetModelValueY(float value)
{
	auto selectedModel = GetSelectedModelResource();
	if (selectedModel)
	{
		m_cached_modelResourceValue.y = value;
		if (modelPositionRadioButton->IsChecked->Value)
		{
			selectedModel->position.y = m_cached_modelResourceValue.y;
		}
		else if (modelRotationRadioButton->IsChecked->Value)
		{
			selectedModel->rotation.y = m_cached_modelResourceValue.y;
		}
		else if (modelScaleRadioButton->IsChecked->Value)
		{
			selectedModel->scale.y = m_cached_modelResourceValue.y;
		}
	}
}

void ControlModelPage::SetModelValueZ(float value)
{
	auto selectedModel = GetSelectedModelResource();
	if (selectedModel)
	{
		m_cached_modelResourceValue.z = value;
		if (modelPositionRadioButton->IsChecked->Value)
		{
			selectedModel->position.z = m_cached_modelResourceValue.z;
		}
		else if (modelRotationRadioButton->IsChecked->Value)
		{
			selectedModel->rotation.z = m_cached_modelResourceValue.z;
		}
		else if (modelScaleRadioButton->IsChecked->Value)
		{
			selectedModel->scale.z = m_cached_modelResourceValue.z;
		}
	}
}

float ControlModelPage::GetModelValueX()
{
	auto selectedModel = GetSelectedModelResource();
	switch (m_cached_modelResourceValueId)
	{
	case 0:
		return !selectedModel ? 0.0f : selectedModel->position.x;
	case 1:
		return !selectedModel ? 0.0f : selectedModel->rotation.x;
	case 2:
		return !selectedModel ? 0.0f : selectedModel->scale.x;
	default:
		return 0.0f;
	}
}

float ControlModelPage::GetModelValueY()
{
	auto selectedModel = GetSelectedModelResource();
	switch (m_cached_modelResourceValueId)
	{
	case 0:
		return !selectedModel ? 0.0f : selectedModel->position.y;
	case 1:
		return !selectedModel ? 0.0f : selectedModel->rotation.y;
	case 2:
		return !selectedModel ? 0.0f : selectedModel->scale.y;
	default:
		return 0.0f;
	}
}

float ControlModelPage::GetModelValueZ()
{
	auto selectedModel = GetSelectedModelResource();
	switch (m_cached_modelResourceValueId)
	{
	case 0:
		return !selectedModel ? 0.0f : selectedModel->position.z;
	case 1:
		return !selectedModel ? 0.0f : selectedModel->rotation.z;
	case 2:
		return !selectedModel ? 0.0f : selectedModel->scale.z;
	default:
		return 0.0f;
	}
}
//events

void EnviromentCreator::ControlModelPage::modelResourceAddButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto enginePtr = DirectXPage::GetEngine();

	if (enginePtr)
	{
		enginePtr->GetGraphics().AppendOutput(
			GetUniqueModelResourceName()
		);
		ReloadModelResourceList();
	}
}

void EnviromentCreator::ControlModelPage::modelResourceRemoveButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (GetSelectedModelResource())
	{
		DirectXPage::GetEngine()->GetGraphics().DeleteOutputByCursor();
		ReloadModelResourceList();
	}
}

void EnviromentCreator::ControlModelPage::modelRadioButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	if (modelPositionRadioButton->IsChecked->Value)
	{
		m_cached_modelResourceValueId = 0;
	}
	else if (modelRotationRadioButton->IsChecked->Value)
	{
		m_cached_modelResourceValueId = 1;
	}
	else if (modelScaleRadioButton->IsChecked->Value)
	{
		m_cached_modelResourceValueId = 2;
	}

	modelValueX_TextBox->Text = ModelValueX;
	modelValueY_TextBox->Text = ModelValueY;
	modelValueZ_TextBox->Text = ModelValueZ;
}

void EnviromentCreator::ControlModelPage::modelValueX_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		modelValueX_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		modelValueX_TextBox->Text = ModelValueX;
	}
}

void EnviromentCreator::ControlModelPage::modelValueX_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	ModelValueX = modelValueX_TextBox->Text;
}

void EnviromentCreator::ControlModelPage::modelValueY_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		modelValueY_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		modelValueY_TextBox->Text = ModelValueY;
	}
}

void EnviromentCreator::ControlModelPage::modelValueY_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	ModelValueY = modelValueY_TextBox->Text;
}

void EnviromentCreator::ControlModelPage::modelValueZ_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args)
{
	if (sender->Text->Length() == 0)
	{
		modelValueZ_TextBox->Text = L"0";
	}
	else if (!DirectXPage::RegexFloat(sender->Text))
	{
		modelValueZ_TextBox->Text = ModelValueZ;
	}
}

void EnviromentCreator::ControlModelPage::modelValueZ_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	ModelValueZ = modelValueZ_TextBox->Text;
}

void ControlModelPage::ReloadModelResourceList()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (enginePtr)
	{
		vector<wstring> outputQuery;
		enginePtr->GetGraphics().QueryOutputByContent(
			enginePtr->GetContent().GetModelMeshStreamCursor(),
			outputQuery
		);

		IVector<String^> ^tmpList = ref new Vector<String^>();

		for (auto &output : outputQuery)
			tmpList->Append(ref new String(output.data()));

		modelResourceListbox->ItemsSource = tmpList;
		modelResourceListbox->SelectedIndex = modelResourceListbox->Items->Size - 1;
	}
}

wstring ControlModelPage::GetUniqueModelResourceName()
{
	int nextIndex = 0;
	auto enginePtr = DirectXPage::GetEngine();
	auto vaildName = enginePtr->GetContent().GetModelMeshStreamCursor() + L'-';

	while (enginePtr->GetGraphics().OutputIsExsist(vaildName + nextIndex.ToString()->Data()))
	{
		++nextIndex;
	}

	return vaildName + nextIndex.ToString()->Data();
}
 
ModelMeshStream* ControlModelPage::GetSelectedModelContent()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (!enginePtr)
	{
		return nullptr;
	}
	else
	{
		return enginePtr->GetContent().GetModelMeshStreamByCursor();
	}
}

ModelObject* ControlModelPage::GetSelectedModelResource()
{
	auto enginePtr = DirectXPage::GetEngine();
	if (!enginePtr)
	{
		return nullptr;
	}
	else
	{
		return enginePtr->GetGraphics().GetOutputByCursor();
	}
}

void EnviromentCreator::ControlModelPage::modelResourceListbox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)
{
	auto selectedItem = modelResourceListbox->SelectedItem;
	auto enginePtr = DirectXPage::GetEngine();

	if (enginePtr)
	{
		if (selectedItem)
		{
			enginePtr->GetGraphics().SetOutputCursor(
				selectedItem->ToString()->Data()
			);

			modelPositionRadioButton->IsChecked = true;
			modelPositionRadioButton->IsEnabled = true;
			modelRotationRadioButton->IsEnabled = true;
			modelScaleRadioButton->IsEnabled = true;

			modelValueX_TextBox->IsEnabled = true;
			modelValueY_TextBox->IsEnabled = true;
			modelValueZ_TextBox->IsEnabled = true;
			
			modelValueX_TextBox->Text = ModelValueX;
			modelValueY_TextBox->Text = ModelValueY;
			modelValueZ_TextBox->Text = ModelValueZ;
		}
		else
		{
			modelPositionRadioButton->IsEnabled = false;
			modelRotationRadioButton->IsEnabled = false;
			modelScaleRadioButton->IsEnabled = false;

			modelValueX_TextBox->IsEnabled = false;
			modelValueY_TextBox->IsEnabled = false;
			modelValueZ_TextBox->IsEnabled = false;
		}
	}
}

void EnviromentCreator::ControlModelPage::animationPlayButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	auto selectedModel = GetSelectedModelResource();
	selectedModel->SetToPlay();
}

