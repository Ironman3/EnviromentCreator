﻿#pragma once

#include "Common\StepTimer.h"
//#include <BaseEngine.h>
#include "..\BaseEngine\BaseEngine.h"

// Renders Direct2D and 3D content on the screen.
namespace EnviromentCreator
{
	class EnviromentCreatorMain
	{
	public:
		EnviromentCreatorMain(const std::shared_ptr<Engine::BaseEngine>& deviceEngine);
		~EnviromentCreatorMain();
		void CreateWindowSizeDependentResources();
		void StartRenderLoop();
		void StopRenderLoop();
		Concurrency::critical_section& GetCriticalSection() { return m_criticalSection; }

		// IDeviceNotify
		virtual void OnDeviceLost();
		virtual void OnDeviceRestored();

	private:
		void ProcessInput();
		void Update();
		bool Render();

		// Cached pointer to device resources.
		std::shared_ptr<Engine::BaseEngine> m_deviceEngine;

		// TODO: Replace with your own content renderers.

		Windows::Foundation::IAsyncAction^ m_renderLoopWorker;
		Concurrency::critical_section m_criticalSection;

		// Rendering loop timer.
		DX::StepTimer m_timer;

		// Track current input pointer position.
		float m_pointerLocationX;
	};
}