﻿//
// ControlModelPage.xaml.h
// Declaration of the ControlModelPage class
//

#pragma once

#include "ControlModelPage.g.h"
#include "..\BaseEngine\BaseEngine.h"

namespace EnviromentCreator
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ControlModelPage sealed
	{
	public:
		ControlModelPage();

	public:
		property Platform::String^ ModelValueX
		{
			Platform::String^ get() { return GetModelValueX().ToString(); }
			void set(Platform::String^ value) { SetModelValueX((float)_wtof(value->Data())); }
		}

		property Platform::String^ ModelValueY
		{
			Platform::String^ get() { return GetModelValueY().ToString(); }
			void set(Platform::String^ value) { SetModelValueY((float)_wtof(value->Data())); }
		}

		property Platform::String^ ModelValueZ
		{
			Platform::String^ get() { return GetModelValueZ().ToString(); }
			void set(Platform::String^ value) { SetModelValueZ((float)_wtof(value->Data())); }
		}
		void SetModelValueX(
			_In_ float value
		);
		void SetModelValueY(
			_In_ float value
		);
		void SetModelValueZ(
			_In_ float value
		);
		float GetModelValueX();
		float GetModelValueY();
		float GetModelValueZ();

	private:
		void modelResourceAddButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void modelResourceRemoveButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void modelRadioButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void modelValueX_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void modelValueX_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void modelValueY_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void modelValueY_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void modelValueZ_TextBox_TextChanging(Windows::UI::Xaml::Controls::TextBox^ sender, Windows::UI::Xaml::Controls::TextBoxTextChangingEventArgs^ args);
		void modelValueZ_TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void modelResourceListbox_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e);
		void animationPlayButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

	private:
		void ReloadModelResourceList();

		std::wstring						GetUniqueModelResourceName();
		Engine::Stream::ModelMeshStream*	GetSelectedModelContent();
		Engine::Resource::ModelObject*		GetSelectedModelResource();

	private:
		Windows::Foundation::IAsyncAction^								m_idleLoopWorker;
		Engine::Stream::ModelMeshStream*								m_cached_modelContent;
		DirectX::XMFLOAT3												m_cached_modelResourceValue;
		int																m_cached_modelResourceValueId;
	};
}
