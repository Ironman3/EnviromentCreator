﻿#include "pch.h"
#include "BaseEngine.h"

using namespace Engine;
using namespace Engine::Context;
using namespace Engine::Content;
using namespace Engine::Graphics;
using namespace Engine::Physics;
using namespace Engine::Message;

using namespace std;
using namespace Windows::UI::Core;

using namespace DirectX;

BaseEngine::BaseEngine() 
{
	m_context = unique_ptr<BaseContext>(
		new BaseContext()
		);
	m_content = unique_ptr<BaseContent>(
		new BaseContent()
		);
	m_graphics = unique_ptr<BaseGraphics>(
		new BaseGraphics(m_context.get(), m_content.get())
		);
	m_physics = unique_ptr<BasePhysics>(
		new BasePhysics(m_graphics.get())
		);
	m_message = unique_ptr<BaseMessage>(
		new BaseMessage()
		);
}

BaseEngine::~BaseEngine()
{
}

void BaseEngine::InitializeEngine(
	_In_ Windows::UI::Xaml::Controls::SwapChainPanel ^swapChainPanel
)
{
	m_context->CreateContext(swapChainPanel);

	//Raster default
	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = true;

	rasterizerDesc.CullMode = D3D11_CULL_NONE;


	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;

	m_graphics->CreateResourceRasterizerState(
		L"default",
		rasterizerDesc
	);
	m_graphics->UseResource(Graphics::RESOURCE_TYPE::RasterizerState, L"default");


	//Depth default
	D3D11_DEPTH_STENCIL_DESC dStencilDesc;
	dStencilDesc.DepthEnable = true;
	dStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	dStencilDesc.StencilEnable = true;
	dStencilDesc.StencilReadMask = 0xFF;
	dStencilDesc.StencilWriteMask = 0xFF;

	dStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	dStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	m_graphics->CreateResourceDepthStencilState(
		L"default",
		dStencilDesc
	);
	//m_graphics->UseResource(Graphics::RESOURCE_TYPE::DepthStencilState, L"default");

	//Sampler default
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(D3D11_SAMPLER_DESC));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.MaxAnisotropy = 1;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.BorderColor[0] = 0.0f;
	sampDesc.BorderColor[1] = 0.0f;
	sampDesc.BorderColor[2] = 0.0f;
	sampDesc.BorderColor[3] = 0.0f;

	m_graphics->CreateResourceSamplerState(
		L"default",
		sampDesc
	);
	m_graphics->UseResource(Graphics::RESOURCE_TYPE::SamplerState, L"default");

	//Blend default
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0].BlendEnable = false;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0F;

	m_graphics->CreateResourceBlendState(
		L"default",
		blendDesc
	);
	//m_graphics->UseResource(Graphics::RESOURCE_TYPE::BlendState, L"default");

	//Input structure
	vector<D3D11_INPUT_ELEMENT_DESC> vsInputElements =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	//Shader_1
	m_graphics->CreateResourceVertexShaderFromFile(
		L"vs_light_diffuse", 
		L"BaseEngine/vs_light_diffuse.cso", 
		vsInputElements.data(), 
		vsInputElements.size()
	);
	m_graphics->CreateResourcePixelShaderFromFile(
		L"ps_light_diffuse", 
		L"BaseEngine/ps_light_diffuse.cso"
	);

	//Shader_2
	m_graphics->CreateResourceVertexShaderFromFile(
		L"vs_light_specular",
		L"BaseEngine/vs_light_specular.cso",
		vsInputElements.data(),
		vsInputElements.size()
	);
	m_graphics->CreateResourcePixelShaderFromFile(
		L"ps_light_specular",
		L"BaseEngine/ps_light_specular.cso"
	);

	m_graphics->UseResource(Graphics::RESOURCE_TYPE::VertexShader, L"vs_light_diffuse");
	m_graphics->UseResource(Graphics::RESOURCE_TYPE::PixelShader, L"ps_light_diffuse");
}

void BaseEngine::ResizeOutput(
	_In_ Windows::Foundation::Size &size
)
{
	m_context->UpdateScreenSize(size);

	auto outputSize = m_context->GetScreenSize();
	float aspectRatio = outputSize.Width / outputSize.Height;
	float fovAngleY = 70.0f * XM_PI / 180.0f;

	if (aspectRatio < 1.0f)
		fovAngleY *= 2.0f;

	m_graphics->GetCameraByCursor()->SetProjection(
		fovAngleY,
		aspectRatio, 
		0.01f, 
		10000.0f
	);
}

void BaseEngine::GraphicsLoop()
{
	m_context->ClearBackBuffer();
	m_graphics->DrawScene();
}

void BaseEngine::PhysicsLoop()
{
	m_graphics->UpdateScene();
}