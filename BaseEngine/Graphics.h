#pragma once

#include "Context.h"
#include "Content.h"
#include "Camera.h"
#include "Light.h"

namespace Engine
{
	namespace Graphics
	{
		enum RESOURCE_TYPE : uint8
		{
			RasterizerState = 0,
			DepthStencilState,
			SamplerState,
			BlendState,
			VertexShader,
			PixelShader,
			Camera
		};

		class BaseGraphics
		{
		private:
			BaseGraphics() : m_contextPtr(nullptr), m_contentPtr(nullptr) {}

		public:
			ENGINE_API BaseGraphics(
				_In_ Context::BaseContext *const p_context,
				_In_ Content::BaseContent *const m_contentPtr
			);
			ENGINE_API ~BaseGraphics() {}

			/// <summary>StringCommand: string(resource name)</summary>
			ENGINE_API void CreateResourceRasterizerState(
				_In_ std::wstring uniqueName,
				_In_ const D3D11_RASTERIZER_DESC &rasterizerDesc
			);
			/// <summary>StringCommand: string(resource name)</summary>
			ENGINE_API void CreateResourceDepthStencilState(
				_In_ std::wstring uniqueName,
				_In_ const D3D11_DEPTH_STENCIL_DESC &depthStencilDesc
			);
			/// <summary>StringCommand: string(resource name)</summary>
			ENGINE_API void CreateResourceSamplerState(
				_In_ std::wstring uniqueName,
				_In_ const D3D11_SAMPLER_DESC &samplerDesc
			);
			/// <summary>StringCommand: string(resource name)</summary>
			ENGINE_API void CreateResourceBlendState(
				_In_ std::wstring uniqueName,
				_In_ const D3D11_BLEND_DESC &blendDesc
			);
			/// <summary>StringCommand: string(resource name)</summary>
			ENGINE_API void CreateResourceVertexShaderFromFile(
				_In_ const std::wstring &uniqueName,
				_In_ const std::wstring &fullName,
				_In_ const D3D11_INPUT_ELEMENT_DESC layoutDesc[],
				_In_ uint32 layoutDescNumElements
			);
			/// <summary>StringCommand: string(resource name)</summary>
			ENGINE_API void CreateResourcePixelShaderFromFile(
				_In_ const std::wstring &uniqueName,
				_In_ const std::wstring &fullName
			);
			ENGINE_API void CreateVertexBuffer(
				_In_ Vertex3D *p_vertexData,
				_In_ uint32 num_vertex,
				_Out_ ID3D11Buffer **pp_vertexBuffer
			);
			ENGINE_API void CreateIndexBuffer(
				_In_ VertexIndex *p_indexData,
				_In_ uint32 num_index,
				_Out_ ID3D11Buffer **pp_indexBuffer
			);
			ENGINE_API void CreateTextureFromDataSource(
				_In_ const D3D11_SUBRESOURCE_DATA &dataSource,
				_In_ const D3D11_TEXTURE2D_DESC &textureDesc,
				_Out_ ID3D11Texture2D **pp_texture2D
			);
			ENGINE_API void CreateSubResource(
				_In_ ID3D11Buffer **pp_buffer,
				_In_ uint32 dataSize
			);
			ENGINE_API void CreateShaderResourceView(
				_In_ ID3D11Resource *p_resource,
				_In_ const D3D11_SHADER_RESOURCE_VIEW_DESC *p_srvDesc,
				_Out_ ID3D11ShaderResourceView **pp_shaderResourceView
			);

			///<summary>StringCommand: string-string(resource name | resouce type(camera, vertex-shader, pixel-shader)</summary>
			ENGINE_API void UseResource(
				_In_ RESOURCE_TYPE rType,
				_In_ const std::wstring uniqueName
			);
			ENGINE_API void UpdateShaderConstants(
				_In_ RESOURCE_TYPE rType,
				_In_ ID3D11Buffer *p_buffer,
				_In_ uint32 slotId,
				_In_ void *p_data
			);
			ENGINE_API void UpdateSubResource(
				_In_ ID3D11Buffer *p_buffer,
				_In_ void *p_data
			);

			ENGINE_API void UpdateScene();
			ENGINE_API void DrawScene();

		public:
			ENGINE_API void AppendCamera(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void DeleteCameraByCursor();
			ENGINE_API void DeleteCameraByName(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void QueryCamera(
				_Inout_ std::vector<std::wstring> &cameraQuery
			);
			ENGINE_API void QueryCamera(
				_Inout_ std::vector<Camera::BaseCamera*> &cameraQuery
			);
			ENGINE_API void SetCameraCursor(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API bool CameraIsExsist(
				_In_ const std::wstring &uniqueName
			);

			std::wstring GetCameraCursor()
			{
				return m_map_camera.GetCursor();
			}

			uint32 GetCameraNumber()
			{
				return static_cast<uint32>(m_map_camera.GetDeviceNumber());
			}

			Camera::BaseCamera* GetCameraByCursor()
			{
				return GetCameraByName(m_map_camera.GetCursor());
			}

			Camera::BaseCamera* GetCameraByName(
				_In_ const std::wstring &uniqueName
			)
			{
				return m_map_camera.GetDevice(uniqueName);
			}
		public:
			ENGINE_API void AppendOutput(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void DeleteOutputByContent();
			ENGINE_API void DeleteOutputByCursor();
			ENGINE_API void DeleteOutputByName(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void QueryOutput(
				_Inout_ std::vector<std::wstring> &outputQuery
			);
			ENGINE_API void QueryOutput(
				_Inout_ std::vector<Resource::ModelObject*> &outputQuery
			);
			ENGINE_API void QueryOutputByContent(
				_In_ const std::wstring &uniqueName,
				_Inout_ std::vector<std::wstring> &outputQuery
			);
			ENGINE_API void QueryOutputByContent(
				_In_ const std::wstring &uniqueName,
				_Inout_ std::vector<Resource::ModelObject*> &outputQuery
			);
			ENGINE_API void SetOutputCursor(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API bool OutputIsExsist(
				_In_ const std::wstring &uniqueName
			);

			std::wstring GetOutputCursor()
			{
				return m_map_output.GetCursor();
			}

			uint32 GetOutputNumber()
			{
				return static_cast<uint32>(m_map_camera.GetDeviceNumber());
			}

			Resource::ModelObject* GetOutputByCursor()
			{
				return GetOutputByName(m_map_output.GetCursor());
			}

			Resource::ModelObject* GetOutputByName(
				_In_ const std::wstring &uniqueName
			)
			{
				return m_map_output.GetDevice(uniqueName);
			}
		public:
			ENGINE_API void AppendLight(
				_In_ const std::wstring &uniqueName,
				_In_ Light::LIGHT_TYPES type
			);
			ENGINE_API void DeleteLightByCursor();
			ENGINE_API void DeleteLightByName(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void QueryLight(
				_Inout_ std::vector<std::wstring> &lightQuery
			);
			ENGINE_API void QueryLight(
				_Inout_ std::vector<Light::BaseLight*> &lightQuery
			);
			ENGINE_API void SetLightCursor(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API bool LightIsExsist(
				_In_ const std::wstring &uniqueName
			);

			std::wstring GetLightCursor()
			{
				return m_map_light.GetCursor();
			}

			uint32 GetLightNumber()
			{
				return static_cast<uint32>(m_map_light.GetDeviceNumber());
			}

			Light::BaseLight* GetLightByCursor()
			{
				return GetLightByName(m_map_light.GetCursor());
			}

			Light::BaseLight* GetLightByName(
				_In_ const std::wstring &uniqueName
			)
			{
				return m_map_light.GetDevice(uniqueName);
			}
		public:
			Resource::TextureObject* CreateResourceTextureObject(
				_In_ const D3D11_TEXTURE2D_DESC &textureDesc,
				_In_ Stream::BitmapStream *p_bitmapStream
			);
			Resource::ModelObject* CreateResourceModelObject(
				_In_ const std::wstring &assetName,
				_In_ Stream::ModelMeshStream *p_modelMeshStream
			);

			void DrawModelObject(
				_In_ Resource::ModelObject *p_mObject
			);
			void DrawIndexed(
				_In_ ID3D11Buffer *p_vertexBuffer,
				_In_ ID3D11Buffer *p_indexBuffer,
				_In_ uint32 vertexNum,
				_In_ uint32 indexNum,
				_In_ uint32 frameIndex,
				_In_opt_ ID3D11ShaderResourceView *p_srvTexture
			);

		private:
			void AddModelResourceByContent(
				_In_ const std::wstring &assetName,
				_In_ const std::wstring &uniqueName,
				_In_ Stream::ModelMeshStream *p_modelMeshStream
			);

		private:
			Devices::DeviceHandler<Resource::Bag::RasterizerBag>	m_map_rasterizerState;
			Devices::DeviceHandler<Resource::Bag::DepthStencilBag>	m_map_depthStencilState;
			Devices::DeviceHandler<Resource::Bag::SamplerBag>		m_map_samplerState;
			Devices::DeviceHandler<Resource::Bag::BlendBag>			m_map_blendState;
			Devices::DeviceHandler<Resource::Bag::VertexShaderBag>	m_map_vertexShader;
			Devices::DeviceHandler<Resource::Bag::PixelShaderBag>	m_map_pixelShader;

			Devices::DeviceHandler<Camera::BaseCamera>				m_map_camera;
			Devices::DeviceHandler<Resource::ModelObject>			m_map_output;
			Devices::DeviceHandler<Light::BaseLight>				m_map_light;
			std::unique_ptr<Camera::BaseCamera>						m_default_cam;
			std::unique_ptr<Resource::TextureObject>				m_default_tex;

			Microsoft::WRL::ComPtr<ID3D11Buffer>					m_buffer_worldViewProj;
			Microsoft::WRL::ComPtr<ID3D11Buffer>					m_buffer_lights;

		private:
			Context::BaseContext *const								m_contextPtr;
			Content::BaseContent *const								m_contentPtr;
		};
	}
}


