﻿#pragma once

#include "Content.h"
#include "Context.h"
#include "Graphics.h"
#include "Physics.h"
#include "Message.h"

namespace Engine
{
	class BaseEngine
	{
	public:
		ENGINE_API BaseEngine();
		ENGINE_API ~BaseEngine();

		ENGINE_API void InitializeEngine(
			_In_ Windows::UI::Xaml::Controls::SwapChainPanel ^swapChainPanel
		);
		ENGINE_API void ResizeOutput(
			_In_ Windows::Foundation::Size &size
		);
		ENGINE_API void GraphicsLoop();
		ENGINE_API void PhysicsLoop();

		Context::BaseContext& GetContext() { return *m_context.get(); }
		Content::BaseContent& GetContent() { return *m_content.get(); }
		Graphics::BaseGraphics& GetGraphics() { return *m_graphics.get(); }
		Physics::BasePhysics& GetPhysics() { return *m_physics.get(); }
		Message::BaseMessage& GetMessage() { return *m_message.get(); }

	public:
		__declspec(property(get = GetContextProvider)) Context::BaseContext&	Context;
		__declspec(property(get = GetContentLoader)) Content::BaseContent&		Content;
		__declspec(property(get = GetGraphicsDevice)) Graphics::BaseGraphics&	Graphics;
		__declspec(property(get = GetPhysicsEngine)) Physics::BasePhysics&		Physics;
		__declspec(property(get = GetMessage)) Message::BaseMessage&			Message;

	public:
	/*	ENGINE_API void SendMessage(Message::VE_Message &msg)
		{
			m_msgBox = msg;
		}

		ENGINE_API void ReadMessage(Message::VE_Message &msg)
		{
			msg = m_msgBox;
			m_msgBox = Message::VE_Message();
		}*/
	private:
		Microsoft::WRL::ComPtr<ID3D11Buffer>	m_lightBuffer;

	private:
		std::unique_ptr<Context::BaseContext>	m_context;
		std::unique_ptr<Content::BaseContent>	m_content;
		std::unique_ptr<Graphics::BaseGraphics>	m_graphics;
		std::unique_ptr<Physics::BasePhysics>	m_physics;
		std::unique_ptr<Message::BaseMessage>	m_message;
	};
}