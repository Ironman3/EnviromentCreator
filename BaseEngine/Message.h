#pragma once

#include "Common.h"
#include "DeviceHandler.h"

namespace Engine
{
	namespace Message
	{
		class MessageThread
		{
		public:
			void SendMsg(
				_In_ int msg
			)
			{
				m_msgList.push_back(msg);
			}

			int ReadMsg()
			{
				if (m_msgList.empty())
					return 0;

				auto tmp = m_msgList.back();
				m_msgList.pop_back();

				return tmp;
			}
		private:
			std::list<int> m_msgList;
		};

		class BaseMessage
		{
		public:
			BaseMessage() {}

			void CreateThread(
				_In_ const std::wstring &uniqueName
			)
			{
				m_map_messageThread.Append(
					uniqueName,
					new MessageThread()
				);
			}

			void DeleteThread(
				_In_ const std::wstring &uniqueName
			)
			{
				m_map_messageThread.Delete(uniqueName);
			}

			void SendMessage(
				_In_ const std::wstring &uniqueName,
				_In_ int msg
			)
			{
				if (!m_map_messageThread.DeviceIsExsist(uniqueName))
					ThrowIfFailed(E_FAIL);

				m_map_messageThread.GetDevice(uniqueName)->SendMsg(msg);
			}

			int ReadMessage(
				_In_ const std::wstring &uniqueName
			)
			{
				int msg = -1;
				if(m_map_messageThread.DeviceIsExsist(uniqueName))
					msg = m_map_messageThread.GetDevice(uniqueName)->ReadMsg();

				return msg;
			}
		private:
			Devices::DeviceHandler<MessageThread> m_map_messageThread;
		};
	}
}
