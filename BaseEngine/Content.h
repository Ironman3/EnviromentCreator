#pragma once

#include "Common.h"
#include "DeviceHandler.h"
#include "PluginBMP.h"
#include "PluginMD3.h"

namespace Engine
{
	namespace Content
	{
		class BaseContent
		{
		public:
			ENGINE_API BaseContent();
			ENGINE_API ~BaseContent();

		public:
			ENGINE_API void LoadBitmapStream(
				_In_ const std::wstring &fullPath
			);
			ENGINE_API void BuildBitmapStream(
				_In_ const std::wstring &uniqueName,
				_In_ FileData ^fileData
			);
			ENGINE_API void DeleteBitmapStreamByCursor();
			ENGINE_API void DeleteBitmapStreamByName(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void QueryBitmapStream(
				_Inout_ std::vector<std::wstring> &query
			);
			ENGINE_API void QueryBitmapStream(
				_Inout_ std::vector<Stream::BitmapStream*> &query
			);
			ENGINE_API void SetBitmapStreamCursor(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API bool BitmapStreamIsExsist(
				_In_ const std::wstring &uniqueName
			);

			std::wstring GetBitmapStreamCursor()
			{
				return m_map_bitmapStream.GetCursor();
			}

			uint32 GetBitmapStreamNumber()
			{
				return static_cast<uint32>(m_map_bitmapStream.GetDeviceNumber());
			}

			Stream::BitmapStream* GetBitmapStreamByCursor()
			{
				return GetBitmapStreamByName(m_map_bitmapStream.GetCursor());
			}

			Stream::BitmapStream* GetBitmapStreamByName(
				_In_ const std::wstring &uniqueName
			)
			{
				return m_map_bitmapStream.GetDevice(uniqueName);
			}
		public:
			ENGINE_API void LoadModelMeshStream(
				_In_ const std::wstring &fullPath
			);
			ENGINE_API void BuildModelMeshStream(
				_In_ const std::wstring &uniqueName,
				_In_ FileData ^fileData
			);
			ENGINE_API void DeleteModelMeshStreamByCursor();
			ENGINE_API void DeleteModelMeshStreamByName(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API void QueryModelMeshStream(
				_Inout_ std::vector<std::wstring> &query
			);
			ENGINE_API void QueryModelMeshStream(
				_Inout_ std::vector<Stream::ModelMeshStream*> &query
			);
			ENGINE_API void SetModelMeshStreamCursor(
				_In_ const std::wstring &uniqueName
			);
			ENGINE_API bool ModelMeshStreamIsExsist(
				_In_ const std::wstring &uniqueName
			);

			std::wstring GetModelMeshStreamCursor()
			{
				return m_map_modelMeshStream.GetCursor();
			}

			uint32 GetModelMeshStreamNumber()
			{
				return static_cast<uint32>(m_map_modelMeshStream.GetDeviceNumber());
			}

			Stream::ModelMeshStream* GetModelMeshStreamByCursor()
			{
				return GetModelMeshStreamByName(m_map_modelMeshStream.GetCursor());
			}

			Stream::ModelMeshStream* GetModelMeshStreamByName(
				_In_ const std::wstring &uniqueName
			)
			{
				return m_map_modelMeshStream.GetDevice(uniqueName);
			}
		private:
			std::unique_ptr<Engine::Plugin::Bitmap::LoaderBMP>	m_loader_bmp;
			std::unique_ptr<Engine::Plugin::Model::LoaderMD3>	m_loader_md3;
			
			Devices::DeviceHandler<Stream::BitmapStream>		m_map_bitmapStream;
			Devices::DeviceHandler<Stream::ModelMeshStream>		m_map_modelMeshStream;
		};
	}
}
