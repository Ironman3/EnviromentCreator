#pragma once

#include "Graphics.h"

namespace Engine
{
	namespace Physics
	{
		class BasePhysics
		{
		public:
			ENGINE_API BasePhysics(
				_In_ Graphics::BaseGraphics *const p_graphics
				);
			ENGINE_API ~BasePhysics();

			ENGINE_API void Update();

		private:
			Graphics::BaseGraphics *const m_graphicsPtr;
		};
	}
}
