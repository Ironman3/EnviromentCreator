#include "pch.h"
#include "Graphics.h"

using namespace Engine;
using namespace Engine::Graphics;
using namespace Engine::Context;
using namespace Engine::Content;
using namespace Engine::IO::File;
using namespace Engine::Devices;
using namespace Engine::Camera;
using namespace Engine::Light;
using namespace Engine::Stream;
using namespace Engine::Resource;
using namespace Engine::Resource::Bag;

using namespace std;

using namespace DirectX;

BaseGraphics::BaseGraphics(
	_In_ BaseContext *const p_context,
	_In_ BaseContent *const p_content

) : m_contextPtr(p_context), m_contentPtr(p_content)
{
	AppendCamera(L"base_camera");
	SetCameraCursor(L"base_camera");

	m_default_cam = unique_ptr<BaseCameraFPS>(
		BaseCameraFPS::CreateCamera()
		);

	ID3D11Buffer *wvpBufferPtr = nullptr;
	CreateSubResource(
		&wvpBufferPtr,
		sizeof(ShaderModelViewProj)
	);
	m_buffer_worldViewProj = wvpBufferPtr;

	ID3D11Buffer *lighstBufferPtr = nullptr;
	CreateSubResource(
		&lighstBufferPtr,
		sizeof(ShaderLights)
	);
	m_buffer_lights = lighstBufferPtr;

	//===========================
	auto bitmapStream = unique_ptr<BitmapStream>(
		new BitmapStream()
		);
	bitmapStream->width = 2;
	bitmapStream->height = 2;
	byte *bitmapArray = new byte[2 * 2 * 3];
	memset(bitmapArray, 255, 2 * 2 * 3);
	bitmapStream->p_array_byte = bitmapArray;
	
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	m_default_tex = unique_ptr<TextureObject>(
		CreateResourceTextureObject(texDesc, bitmapStream.get())
		);
}

void BaseGraphics::CreateResourceRasterizerState(
	_In_ wstring uniqueName,
	_In_ const D3D11_RASTERIZER_DESC &rasterizerDesc
)
{
	if (m_map_rasterizerState.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	ID3D11RasterizerState *rsStatePtr = nullptr;
	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateRasterizerState(&rasterizerDesc, &rsStatePtr)
	);
	
	m_map_rasterizerState.Append(
		uniqueName,
		new RasterizerBag(rsStatePtr)
	);
}

void BaseGraphics::CreateResourceDepthStencilState(
	_In_ wstring uniqueName,
	_In_ const D3D11_DEPTH_STENCIL_DESC &depthStencilDesc
)
{
	if (m_map_depthStencilState.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	ID3D11DepthStencilState *dsStatePtr = nullptr;
	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateDepthStencilState(&depthStencilDesc, &dsStatePtr)
	);

	m_map_depthStencilState.Append(
		uniqueName,
		new DepthStencilBag(dsStatePtr)
	);
}

void BaseGraphics::CreateResourceSamplerState(
	_In_ wstring uniqueName,
	_In_ const D3D11_SAMPLER_DESC &samplerDesc
)
{
	if (m_map_samplerState.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	ID3D11SamplerState *sStatePtr = nullptr;
	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateSamplerState(&samplerDesc, &sStatePtr)
	);

	m_map_samplerState.Append(
		uniqueName,
		new SamplerBag(sStatePtr, 0, 1)
	);
}

void BaseGraphics::CreateResourceBlendState(
	_In_ std::wstring uniqueName,
	_In_ const D3D11_BLEND_DESC &blendDesc
)
{
	if (m_map_blendState.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	ID3D11BlendState *bsStatePtr = nullptr;
	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateBlendState(&blendDesc, &bsStatePtr)
	);

	m_map_blendState.Append(
		uniqueName,
		new BlendBag(bsStatePtr, 0, nullptr)
	);
}

void BaseGraphics::CreateResourceVertexShaderFromFile(
	_In_ const std::wstring &uniqueName,
	_In_ const std::wstring &fullName,
	_In_ const D3D11_INPUT_ELEMENT_DESC layoutDesc[],
	_In_ uint32 layoutDescNumElements
)
{
	if (m_map_vertexShader.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	FileData ^vsBlob;
	try
	{
		vsBlob = ReadData(fullName);
	}
	catch (Platform::Exception^)
	{
		ThrowIfFailed(E_FAIL);
	}

	auto d3dDevice = m_contextPtr->GetD3DDevice();

	ID3D11VertexShader *vsPtr = nullptr;
	ThrowIfFailed(
		d3dDevice->CreateVertexShader(
			vsBlob->Data,
			vsBlob->Length,
			nullptr,
			&vsPtr
		)
	);

	ID3D11InputLayout *iLayoutPtr = nullptr;
	ThrowIfFailed(
		d3dDevice->CreateInputLayout(
			layoutDesc,
			layoutDescNumElements,
			vsBlob->Data,
			vsBlob->Length,
			&iLayoutPtr
		)
	);

	m_map_vertexShader.Append(
		uniqueName,
		new VertexShaderBag(vsPtr, iLayoutPtr)
	);
}

void BaseGraphics::CreateResourcePixelShaderFromFile(
	_In_ const std::wstring &uniqueName,
	_In_ const std::wstring &fullName
)
{
	if (m_map_pixelShader.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	FileData ^psBlob;
	try
	{
		psBlob = ReadData(fullName);
	}
	catch (Platform::Exception^)
	{
		ThrowIfFailed(E_FAIL);
	}

	auto d3dDevice = m_contextPtr->GetD3DDevice();

	ID3D11PixelShader *psPtr = nullptr;
	ThrowIfFailed(
		d3dDevice->CreatePixelShader(
			psBlob->Data,
			psBlob->Length,
			nullptr,
			&psPtr
		)
	);

	m_map_pixelShader.Append(
		uniqueName,
		new PixelShaderBag(psPtr)
	);
}

void BaseGraphics::CreateTextureFromDataSource(
	_In_ const D3D11_SUBRESOURCE_DATA &dataSource,
	_In_ const D3D11_TEXTURE2D_DESC &textureDesc,
	_Out_ ID3D11Texture2D **pp_texture2D
)
{
	if (!pp_texture2D)
		ThrowIfFailed(E_FAIL);

	m_contextPtr->GetD3DDevice()->CreateTexture2D(&textureDesc, &dataSource, pp_texture2D);
}

void BaseGraphics::CreateSubResource(
	_In_ ID3D11Buffer **pp_buffer,
	_In_ uint32 dataSize
)
{
	if (!pp_buffer)
		ThrowIfFailed(CO_E_FIRST);

	CD3D11_BUFFER_DESC cbDesc(
		dataSize,
		D3D11_BIND_CONSTANT_BUFFER
	);

	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateBuffer(
			&cbDesc,
			nullptr,
			pp_buffer
		)
	);
}

void BaseGraphics::CreateShaderResourceView(
	_In_ ID3D11Resource *p_resource,
	_In_ const D3D11_SHADER_RESOURCE_VIEW_DESC *p_srvDesc,
	_Out_ ID3D11ShaderResourceView **pp_shaderResourceView
)
{
	auto d3dDevPtr = m_contextPtr->GetD3DDevice();
	d3dDevPtr->CreateShaderResourceView(
		p_resource,
		p_srvDesc, 
		pp_shaderResourceView
	);
}

void BaseGraphics::UseResource(
	_In_ RESOURCE_TYPE rType,
	_In_ const std::wstring uniqueName
)
{
	auto d3dDevConPtr = m_contextPtr->GetD3DDeviceContext();

	if (rType == Engine::Graphics::RasterizerState)
	{
		RasterizerBag *rsDevicePtr = m_map_rasterizerState.GetDevice(uniqueName);
		if (rsDevicePtr) d3dDevConPtr->RSSetState(rsDevicePtr->rasterizerState.Get());
	}
	else if (rType == Engine::Graphics::DepthStencilState)
	{
		DepthStencilBag *dsDevicePtr = m_map_depthStencilState.GetDevice(uniqueName);
		if (dsDevicePtr) d3dDevConPtr->OMSetDepthStencilState(dsDevicePtr->depthStencilState.Get(), 0);
	}
	else if (rType == Engine::Graphics::SamplerState)
	{
		m_map_samplerState.SetCursor(uniqueName);
	}
	else if (rType == Engine::Graphics::BlendState)
	{
		BlendBag *bsDevicePtr = m_map_blendState.GetDevice(uniqueName);
		if (bsDevicePtr)
		{
			d3dDevConPtr->OMSetBlendState(
				bsDevicePtr->blendState.Get(),
				reinterpret_cast<FLOAT*>(bsDevicePtr->p_blendFactor),
				bsDevicePtr->sampleMask
			);
		}
	}
	else if (rType == Engine::Graphics::VertexShader)
	{
		m_map_vertexShader.SetCursor(uniqueName);
	}
	else if (rType == Engine::Graphics::PixelShader)
	{
		m_map_pixelShader.SetCursor(uniqueName);
	}
	else
	{
		ThrowIfFailed(E_FAIL);
	}
}

void BaseGraphics::UpdateShaderConstants(
	_In_ RESOURCE_TYPE rType,
	_In_ ID3D11Buffer *p_buffer,
	_In_ uint32 slotId,
	_In_ void *p_data
)
{
	auto d3dDevConPtr = m_contextPtr->GetD3DDeviceContext();

	switch (rType)
	{
	case Engine::Graphics::VertexShader:

		UpdateSubResource(p_buffer, p_data);
		d3dDevConPtr->VSSetConstantBuffers(slotId, 1, &p_buffer);

		break;
	case Engine::Graphics::PixelShader:

		UpdateSubResource(p_buffer, p_data);
		d3dDevConPtr->PSSetConstantBuffers(slotId, 1, &p_buffer);

		break;
	default:
		ThrowIfFailed(E_FAIL);
	}
}

void BaseGraphics::UpdateSubResource(
	_In_ ID3D11Buffer *p_buffer,
	_In_ void *p_data
)
{
	auto d3dDevConPtr = m_contextPtr->GetD3DDeviceContext();

	d3dDevConPtr->UpdateSubresource(
		p_buffer,
		0,
		nullptr,
		p_data,
		0,
		0
	);
}

void BaseGraphics::UpdateScene()
{
	vector<ModelObject*> outputQuery;
	m_map_output.QueryList(outputQuery);

	for (auto &output : outputQuery)
		output->MoveToNextFrame();
}

void BaseGraphics::AddModelResourceByContent(
	_In_ const wstring &assetName,
	_In_ const wstring &uniqueName,
	_In_ ModelMeshStream *p_modelMeshStream
)
{
	if (m_map_output.DeviceIsExsist(uniqueName))
		ThrowIfFailed(E_FAIL);

	m_map_output.Append(
		uniqueName,
		CreateResourceModelObject(assetName, p_modelMeshStream)
	);
}

void BaseGraphics::DrawScene()
{
	auto d3dDevConPtr = m_contextPtr->GetD3DDeviceContext();

	if (!m_map_vertexShader.DeviceIsExsist(m_map_vertexShader.GetCursor()))
		ThrowIfFailed(E_FAIL);

	VertexShaderBag *vShaderBagPtr = m_map_vertexShader.GetDevice(
		m_map_vertexShader.GetCursor()
	);
	d3dDevConPtr->IASetInputLayout(
		vShaderBagPtr->inputLayout.Get()
	);
	d3dDevConPtr->VSSetShader(
		vShaderBagPtr->vertexShader.Get(),
		nullptr,
		0
	);

	if (!m_map_pixelShader.DeviceIsExsist(m_map_pixelShader.GetCursor()))
		ThrowIfFailed(E_FAIL);

	PixelShaderBag *pShaderBagPtr = m_map_pixelShader.GetDevice(
		m_map_pixelShader.GetCursor()
	);
	d3dDevConPtr->PSSetShader(
		pShaderBagPtr->pixelShader.Get(),
		nullptr,
		0
	);

	//
	vector<BaseLight*> lightQuery;
	m_map_light.QueryList(lightQuery);
	ShaderLights lights;
	ZeroMemory(&lights, sizeof(lights));

	auto itLightBegin = lightQuery.begin();
	uint32 itCounter = 0;
	for (auto itLight = itLightBegin; itLight != lightQuery.end() && itCounter < LIGHT_MAX; ++itLight)
	{
		auto lightPos = (*itLight)->GetPosition();
		auto lightDir = (*itLight)->GetDirection();
		auto lightCol = (*itLight)->GetColor_RGBA();

		lights.lightArray[itCounter].light_pos = XMFLOAT4(lightPos.x, lightPos.y, lightPos.z, 0.0f);
		lights.lightArray[itCounter].light_dir = XMFLOAT4(lightDir.x, lightDir.y, lightDir.z, 0.0f);
		lights.lightArray[itCounter++].light_col = lightCol;

		++lights.lightHeader.info_meta[0];
	}
	lights.lightHeader.light_meta = XMFLOAT4(0.2, 0, 0, 0);

	auto size = sizeof(ShaderLights);

	UpdateShaderConstants(
		RESOURCE_TYPE::PixelShader,
		m_buffer_lights.Get(),
		0,
		&lights
	);
	//

	vector<ModelObject*> outputQuery;
	m_map_output.QueryList(outputQuery);

	for (auto &output : outputQuery)
		DrawModelObject(output);
}

#pragma region CameraDevices

void BaseGraphics::AppendCamera(
	_In_ const std::wstring &uniqueName
)
{
	m_map_camera.Append(
		uniqueName,
		BaseCameraFPS::CreateCamera()
	);
}

void BaseGraphics::DeleteCameraByCursor()
{
	m_map_camera.Delete(
		m_map_camera.GetCursor()
	);
}

void BaseGraphics::DeleteCameraByName(
	_In_ const wstring &uniqueName
)
{
	m_map_camera.Delete(uniqueName);
}

void BaseGraphics::QueryCamera(
	_Inout_ vector<wstring> &cameraQuery
)
{
	m_map_camera.QueryList(cameraQuery);
}

void BaseGraphics::QueryCamera(
	_Inout_ vector<BaseCamera*> &cameraQuery
)
{
	m_map_camera.QueryList(cameraQuery);
}

void BaseGraphics::SetCameraCursor(
	_In_ const wstring &uniqueName
)
{
	m_map_camera.SetCursor(uniqueName);
}

bool BaseGraphics::CameraIsExsist(
	_In_ const wstring &uniqueName
)
{
	return m_map_camera.DeviceIsExsist(uniqueName);
}
#pragma endregion

#pragma region Outputs
void BaseGraphics::AppendOutput(
	_In_ const wstring &uniqueName
)
{
	ModelMeshStream *modelMeshStreamPtr = 
		m_contentPtr->GetModelMeshStreamByCursor();

	if (modelMeshStreamPtr)
	{
		AddModelResourceByContent(
			m_contentPtr->GetModelMeshStreamCursor(),
			uniqueName,
			modelMeshStreamPtr
		);
	}
}

void BaseGraphics::DeleteOutputByContent()
{
	vector<wstring> outputNameList;
	m_map_output.QueryList(outputNameList);

	for (auto &outputName : outputNameList)
	{
		auto device = m_map_output.GetDevice(outputName);
		if (device->GetParentUniqueName() == m_contentPtr->GetModelMeshStreamCursor())
			m_map_output.Delete(outputName);
	}
}

void BaseGraphics::DeleteOutputByCursor()
{
	DeleteOutputByName(
		m_map_output.GetCursor()
	);
}

void BaseGraphics::DeleteOutputByName(
	_In_ const std::wstring &uniqueName
)
{
	m_map_output.Delete(uniqueName);
}

void BaseGraphics::QueryOutput(
	_Inout_ vector<wstring> &outputQuery
)
{
	m_map_output.QueryList(outputQuery);
}

void BaseGraphics::QueryOutput(
	_Inout_ vector<ModelObject*> &outputQuery
)
{
	m_map_output.QueryList(outputQuery);
}

void BaseGraphics::QueryOutputByContent(
	_In_ const wstring &uniqueName,
	_Inout_ vector<wstring> &outputQuery
)
{
	vector<wstring> tmpQuery;
	m_map_output.QueryList(tmpQuery);

	for (auto &tmp : tmpQuery)
	{
		auto device = m_map_output.GetDevice(tmp);

		if (device && uniqueName == device->GetParentUniqueName())
			outputQuery.push_back(tmp);
	}
}

void BaseGraphics::QueryOutputByContent(
	_In_ const wstring &uniqueName,
	_Inout_ vector<ModelObject*> &outputQuery
)
{
	vector<ModelObject*> tmpQuery;
	m_map_output.QueryList(tmpQuery);

	for (auto &tmp : tmpQuery)
	{
		if (uniqueName == tmp->GetParentUniqueName())
			outputQuery.push_back(tmp);
	}
}

void BaseGraphics::SetOutputCursor(
	_In_ const wstring &uniqueName
)
{
	m_map_output.SetCursor(uniqueName);
}

bool BaseGraphics::OutputIsExsist(
	_In_ const wstring &uniqueName
)
{
	return m_map_output.DeviceIsExsist(uniqueName);
}
#pragma endregion

#pragma LightDevices
void BaseGraphics::AppendLight(
	_In_ const std::wstring &uniqueName,
	_In_ LIGHT_TYPES type
)
{
	auto lightPtr = new BaseLight(
		type,
		0.0f
	);
	m_map_light.Append(
		uniqueName,
		lightPtr
	);
}

void BaseGraphics::DeleteLightByCursor()
{
	DeleteLightByName(m_map_light.GetCursor());
}

void BaseGraphics::DeleteLightByName(
	_In_ const std::wstring &uniqueName
)
{
	m_map_light.Delete(uniqueName);
}

void BaseGraphics::QueryLight(
	_Inout_ vector<wstring> &lightQuery
)
{
	m_map_light.QueryList(lightQuery);
}

void BaseGraphics::QueryLight(
	_Inout_ vector<BaseLight*> &lightQuery
)
{
	m_map_light.QueryList(lightQuery);
}

void BaseGraphics::SetLightCursor(
	_In_ const std::wstring &uniqueName
)
{
	m_map_light.SetCursor(uniqueName);
}

bool BaseGraphics::LightIsExsist(
	_In_ const std::wstring &uniqueName
)
{
	return m_map_light.DeviceIsExsist(uniqueName);
}
#pragma endregion

void BaseGraphics::DrawModelObject(
	_In_ Resource::ModelObject *p_mObject
)
{
	if (!p_mObject || !GetCameraByCursor())
		ThrowIfFailed(E_FAIL);

	ShaderModelViewProj mvp;
	ZeroMemory(&mvp, sizeof(mvp));

	auto transform = XMMatrixScaling(
		p_mObject->scale.x,
		p_mObject->scale.y,
		p_mObject->scale.z
	) *  XMMatrixRotationRollPitchYaw(
		p_mObject->rotation.x,
		p_mObject->rotation.y,
		p_mObject->rotation.z
	) *  XMMatrixTranslation(
		p_mObject->position.x,
		p_mObject->position.y,
		p_mObject->position.z
	);
	auto cameraPtr = GetCameraByCursor();
	auto viewMat = cameraPtr->ViewMatrix();
	auto projMat = cameraPtr->ProjectionMatrix();
	XMFLOAT3 camPos = cameraPtr->GetEye();
	XMFLOAT3 camDir = cameraPtr->GetLook();

	XMStoreFloat4x4(
		&mvp.model,
		XMMatrixTranspose(transform)
	);
	XMStoreFloat4x4(
		&mvp.view,
		XMMatrixTranspose(XMLoadFloat4x4(&viewMat))
	);
	XMStoreFloat4x4(
		&mvp.proj,
		XMMatrixTranspose(XMLoadFloat4x4(&projMat))
	);
	mvp.align_1 = XMFLOAT4(camPos.x, camPos.y, camPos.z, 1.0f);
	mvp.align_2 = XMFLOAT4(camDir.x, camDir.y, camDir.z, 1.0f);

	UpdateShaderConstants(
		RESOURCE_TYPE::VertexShader,
		m_buffer_worldViewProj.Get(),
		0,
		&mvp
	);

	auto itMeshPartBegin = p_mObject->GetMeshArray();
	auto itMeshPartEnd = itMeshPartBegin + p_mObject->GetMeshNumber();
	for (auto itMeshPart = itMeshPartBegin; itMeshPart < itMeshPartEnd; ++itMeshPart)
	{
		DrawIndexed(
			itMeshPart->GetVertexBuffer(),
			itMeshPart->GetIndexBuffer(),
			itMeshPart->GetVertexNumber(),
			itMeshPart->GetIndexNumber(),
			itMeshPart->GetFrameIndex(),
			nullptr
		);
	}
}

void BaseGraphics::DrawIndexed(
	_In_ ID3D11Buffer *p_vertexBuffer,
	_In_ ID3D11Buffer *p_indexBuffer,
	_In_ uint32 vertexNum,
	_In_ uint32 indexNum,
	_In_ uint32 frameIndex,
	_In_opt_ ID3D11ShaderResourceView *p_srvTexture
)
{
	auto d3dDevConPtr = m_contextPtr->GetD3DDeviceContext();
	auto samplerPtr = m_map_samplerState.GetDevice(m_map_samplerState.GetCursor());

	if (samplerPtr)
	{
		if (!p_srvTexture)
		{
			auto defaultTexPtr = m_default_tex->GetShaderResourceView();
			d3dDevConPtr->PSSetShaderResources(0, 1, &defaultTexPtr);
		}
		else
		{
			d3dDevConPtr->PSSetShaderResources(0, 1, &p_srvTexture);
		}

		d3dDevConPtr->PSSetSamplers(
			samplerPtr->slotId,
			samplerPtr->samplerNum,
			&samplerPtr->samplerStates
		);
	}

	UINT32 vertexStride = sizeof(Vertex3D);
	UINT offset = frameIndex * vertexNum * vertexStride;
	d3dDevConPtr->IASetVertexBuffers(
		0,
		1,
		&p_vertexBuffer,
		&vertexStride,
		&offset
	);

	d3dDevConPtr->IASetIndexBuffer(
		p_indexBuffer,
		DXGI_FORMAT_R32_UINT,
		0
	);

	d3dDevConPtr->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	d3dDevConPtr->DrawIndexed(indexNum, 0, 0);
}

TextureObject* BaseGraphics::CreateResourceTextureObject(
	_In_ const D3D11_TEXTURE2D_DESC &textureDesc,
	_In_ BitmapStream *p_bitmapStream
)
{
	D3D11_SUBRESOURCE_DATA rData;
	rData.pSysMem = static_cast<void*>(p_bitmapStream->p_array_byte);
	rData.SysMemPitch = p_bitmapStream->width * 3;
	rData.SysMemSlicePitch = 0;

	D3D11_TEXTURE2D_DESC tmpTexDesc = textureDesc;
	tmpTexDesc.Width = p_bitmapStream->width;
	tmpTexDesc.Height = p_bitmapStream->height;

	ID3D11Texture2D *tex2D_Ptr = nullptr;
	CreateTextureFromDataSource(rData, tmpTexDesc, &tex2D_Ptr);
	if (!tex2D_Ptr)
		ThrowIfFailed(E_FAIL);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;

	ID3D11ShaderResourceView *srvPtr = nullptr;
	CreateShaderResourceView(tex2D_Ptr, &srvDesc, &srvPtr);
	if (!srvPtr)
		ThrowIfFailed(E_FAIL);

	TextureObject *tObjectPtr = new TextureObject();
	tObjectPtr->InitTexture(tex2D_Ptr, srvPtr);

	return tObjectPtr;
}

ModelObject* BaseGraphics::CreateResourceModelObject(
	_In_ const wstring &assetName,
	_In_ ModelMeshStream *p_modelMeshStream
)
{
	ModelObject *mObjectPtr = new ModelObject(assetName, p_modelMeshStream->num_mesh);

	auto itSubMeshBegin = p_modelMeshStream->p_array_subMeshStream;
	for (auto itSubMesh = itSubMeshBegin; itSubMesh < itSubMeshBegin + p_modelMeshStream->num_mesh; ++itSubMesh)
	{
		auto meshObjectPtr = &mObjectPtr->GetMeshArray()[itSubMesh - itSubMeshBegin];

		ID3D11Buffer *vertexBufferPtr = nullptr;
		CreateVertexBuffer(
			itSubMesh->p_array_vertex,
			itSubMesh->num_vertex * itSubMesh->num_frame,
			&vertexBufferPtr
		);
		meshObjectPtr->InitVertexBuffer(vertexBufferPtr, itSubMesh->num_vertex);
		meshObjectPtr->InitFrames(itSubMesh->num_frame);

		ID3D11Buffer *indexBufferPtr = nullptr;
		CreateIndexBuffer(
			itSubMesh->p_array_index,
			itSubMesh->num_index,
			&indexBufferPtr
		);
		meshObjectPtr->InitIndexBuffer(indexBufferPtr, itSubMesh->num_index);
	}

	return mObjectPtr;
}

void BaseGraphics::CreateVertexBuffer(
	_In_ Vertex3D *p_vertexData,
	_In_ uint32 num_vertex,
	_Out_ ID3D11Buffer **pp_vertexBuffer
)
{
	if (!p_vertexData || !pp_vertexBuffer)
		ThrowIfFailed(E_FAIL);

	D3D11_SUBRESOURCE_DATA vbData = { 0 };
	vbData.pSysMem = p_vertexData;
	vbData.SysMemPitch = 0;
	vbData.SysMemSlicePitch = 0;

	CD3D11_BUFFER_DESC vbDesc(
		num_vertex * sizeof(Vertex3D),
		D3D11_BIND_VERTEX_BUFFER
	);

	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateBuffer(
			&vbDesc,
			&vbData,
			pp_vertexBuffer
		)
	);
}

void BaseGraphics::CreateIndexBuffer(
	_In_ VertexIndex *p_indexData,
	_In_ uint32 num_index,
	_Out_ ID3D11Buffer **pp_indexBuffer
)
{
	if (!p_indexData || !pp_indexBuffer)
		ThrowIfFailed(CO_E_FIRST);

	D3D11_SUBRESOURCE_DATA ibData = { 0 };
	ibData.pSysMem = p_indexData;
	ibData.SysMemPitch = 0;
	ibData.SysMemSlicePitch = 0;

	CD3D11_BUFFER_DESC ibDesc(
		num_index * sizeof(VertexIndex),
		D3D11_BIND_INDEX_BUFFER
	);

	ThrowIfFailed(
		m_contextPtr->GetD3DDevice()->CreateBuffer(
			&ibDesc,
			&ibData,
			pp_indexBuffer
		)
	);
}
