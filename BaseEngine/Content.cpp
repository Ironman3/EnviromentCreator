#include "pch.h"
#include "Content.h"

using namespace Engine;
using namespace Engine::Content;
using namespace Engine::Stream;
using namespace Engine::Resource;
using namespace Engine::Plugin::Bitmap;
using namespace Engine::Plugin::Model;

using namespace Platform;

using namespace std;

BaseContent::BaseContent()
{
	m_loader_bmp = unique_ptr<LoaderBMP>(
		new LoaderBMP()
		);
	m_loader_md3 = unique_ptr<LoaderMD3>(
		new LoaderMD3()
		);
}

BaseContent::~BaseContent()
{
}

#pragma region BitmapStream

void BaseContent::LoadBitmapStream(
	_In_ const wstring &fullPath
)
{
	BitmapStream *bitmapStreamPtr = nullptr;

	m_loader_bmp->LoadFromFile(fullPath, &bitmapStreamPtr);
	m_map_bitmapStream.Append(fullPath, bitmapStreamPtr);
}

void BaseContent::BuildBitmapStream(
	_In_ const wstring &uniqueName,
	_In_ FileData ^fileData
)
{
	BitmapStream *bitmapStreamPtr = nullptr;

	m_loader_bmp->BuildFromStream(fileData, &bitmapStreamPtr);
	m_map_bitmapStream.Append(uniqueName, bitmapStreamPtr);
}

void BaseContent::DeleteBitmapStreamByCursor()
{
	DeleteBitmapStreamByName(m_map_bitmapStream.GetCursor());
}

void BaseContent::DeleteBitmapStreamByName(
	_In_ const std::wstring &uniqueName
)
{
	m_map_bitmapStream.Delete(uniqueName);
}

void BaseContent::QueryBitmapStream(
	_Inout_ vector<wstring> &query
)
{
	m_map_bitmapStream.QueryList(query);
}

void BaseContent::QueryBitmapStream(
	_Inout_ vector<BitmapStream*> &query
)
{
	m_map_bitmapStream.QueryList(query);
}

void BaseContent::SetBitmapStreamCursor(
	_In_ const std::wstring &uniqueName
)
{
	m_map_bitmapStream.SetCursor(uniqueName);
}

bool BaseContent::BitmapStreamIsExsist(
	_In_ const std::wstring &uniqueName
)
{
	return m_map_bitmapStream.DeviceIsExsist(uniqueName);
}
#pragma endregion

#pragma region ModelMeshStream

void BaseContent::LoadModelMeshStream(
	_In_ const wstring &fullPath
)
{
	ModelMeshStream *modelMeshStreamPtr = nullptr;

	m_loader_md3->LoadFromFile(fullPath, &modelMeshStreamPtr);
	m_map_modelMeshStream.Append(fullPath, modelMeshStreamPtr);
}

void BaseContent::BuildModelMeshStream(
	_In_ const wstring &uniqueName,
	_In_ FileData ^fileData
)
{
	ModelMeshStream *modelMeshStreamPtr = nullptr;
	
	m_loader_md3->BuildFromStream(fileData, &modelMeshStreamPtr);
	m_map_modelMeshStream.Append(uniqueName, modelMeshStreamPtr);
}

void BaseContent::DeleteModelMeshStreamByCursor()
{
	DeleteModelMeshStreamByName(m_map_modelMeshStream.GetCursor());
}

void BaseContent::DeleteModelMeshStreamByName(
	_In_ const std::wstring &uniqueName
)
{
	m_map_modelMeshStream.Delete(uniqueName);
}

void BaseContent::QueryModelMeshStream(
	_Inout_ vector<wstring> &query
)
{
	m_map_modelMeshStream.QueryList(query);
}

void BaseContent::QueryModelMeshStream(
	_Inout_ vector<ModelMeshStream*> &query
)
{
	m_map_modelMeshStream.QueryList(query);
}

void BaseContent::SetModelMeshStreamCursor(
	_In_ const std::wstring &uniqueName
)
{
	m_map_modelMeshStream.SetCursor(uniqueName);
}

bool BaseContent::ModelMeshStreamIsExsist(
	_In_ const std::wstring &uniqueName
)
{
	return m_map_modelMeshStream.DeviceIsExsist(uniqueName);
}
#pragma endregion
