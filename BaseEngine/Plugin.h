#pragma once

#include "Common.h"

namespace Engine
{
	namespace Plugin
	{
		__interface IPluginBitmap
		{
			void LoadFromFile(
				_In_ std::wstring fullPath,
				_Out_ Stream::BitmapStream **pp_bitmapStream
			);
			void BuildFromStream(
				_In_ FileData ^fileData,
				_Out_ Stream::BitmapStream **pp_bitmapStream
			);
		};
		
		__interface IPluginModel
		{
			void LoadFromFile(
				_In_ std::wstring fullPath,
				_Out_ Stream::ModelMeshStream **pp_modelMeshStream
			);
			void BuildFromStream(
				_In_ FileData ^fileData,
				_Out_ Stream::ModelMeshStream **pp_modelMeshStream
			);
		};
	}
}
