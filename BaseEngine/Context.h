#pragma once

#include "Common.h"

namespace Engine
{
	namespace Context
	{
		class BaseContext
		{
		public:
			ENGINE_API BaseContext();
			ENGINE_API ~BaseContext();

			ENGINE_API void CreateContext(
				_In_ Windows::UI::Xaml::Controls::SwapChainPanel ^wapChainPanel
			);
			ENGINE_API void ValidateDevice();
			ENGINE_API void HandleDeviceLost();
			ENGINE_API void Trim();
			ENGINE_API void ClearBackBuffer();
			ENGINE_API void Present();
			ENGINE_API void UpdateScreenSize(
				_In_ Windows::Foundation::Size size
			);

			// D3D Accessors.
			ID3D11Device2*				GetD3DDevice() const { return m_d3dDevice.Get(); }
			ID3D11DeviceContext2*		GetD3DDeviceContext() const { return m_d3dContext.Get(); }
			IDXGISwapChain1*			GetSwapChain() const { return m_swapChain.Get(); }
			D3D_FEATURE_LEVEL			GetDeviceFeatureLevel() const { return m_d3dFeatureLevel; }
			ID3D11RenderTargetView*		GetBackBufferRenderTargetView() const { return m_d3dRenderTargetView.Get(); }
			ID3D11DepthStencilView*		GetDepthStencilView() const { return m_d3dDepthStencilView.Get(); }
			D3D11_VIEWPORT				GetScreenViewport() const { return m_screenViewport; }

			// D2D Accessors.
			ID2D1Factory2*				GetD2DFactory() const { return m_d2dFactory.Get(); }
			ID2D1Device1*				GetD2DDevice() const { return m_d2dDevice.Get(); }
			ID2D1DeviceContext1*		GetD2DDeviceContext() const { return m_d2dContext.Get(); }
			ID2D1Bitmap1*				GetD2DTargetBitmap() const { return m_d2dTargetBitmap.Get(); }
			IDWriteFactory2*			GetDWriteFactory() const { return m_dwriteFactory.Get(); }
			IWICImagingFactory2*		GetWicImagingFactory() const { return m_wicFactory.Get(); }

			Windows::Foundation::Size	GetScreenSize() { return m_screenSize; }
			Windows::Foundation::Size	GetResoultion() { return m_resoultion; }

		private:
			void CreateDeviceIndependentResources();
			void CreateDeviceResources();
			void CreateWindowSizeDependentResources();

			Windows::UI::Xaml::Controls::SwapChainPanel^	m_swapChainPanel;

			Microsoft::WRL::ComPtr<ID3D11Device3>			m_d3dDevice;
			Microsoft::WRL::ComPtr<ID3D11DeviceContext3>	m_d3dContext;
			Microsoft::WRL::ComPtr<IDXGISwapChain3>			m_swapChain;
			D3D_FEATURE_LEVEL                               m_d3dFeatureLevel;

			// Direct3D rendering objects. Required for 3D.
			Microsoft::WRL::ComPtr<ID3D11RenderTargetView1>	m_d3dRenderTargetView;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView>	m_d3dDepthStencilView;
			D3D11_VIEWPORT									m_screenViewport;

			// Direct2D drawing components.
			Microsoft::WRL::ComPtr<ID2D1Factory3>			m_d2dFactory;
			Microsoft::WRL::ComPtr<ID2D1Device2>			m_d2dDevice;
			Microsoft::WRL::ComPtr<ID2D1DeviceContext2>		m_d2dContext;
			Microsoft::WRL::ComPtr<ID2D1Bitmap1>			m_d2dTargetBitmap;

			// DirectWrite drawing components.
			Microsoft::WRL::ComPtr<IDWriteFactory3>			m_dwriteFactory;
			Microsoft::WRL::ComPtr<IWICImagingFactory2>		m_wicFactory;

			Windows::Foundation::Size						m_screenSize;
			Windows::Foundation::Size						m_resoultion;
		};
	}
}


