#pragma once
#include "Common.h"

namespace Engine
{
	namespace Camera
	{
		class BaseCamera
		{
		protected:
			virtual void Update() = 0;

			DirectX::XMFLOAT4X4		m_projMat;
			DirectX::XMFLOAT4X4		m_viewMat;
			DirectX::XMFLOAT4		m_worldQuat;
			DirectX::XMFLOAT4		m_frameQuat;
			DirectX::XMFLOAT3		m_eyePos;
			DirectX::XMFLOAT3		m_lookDir;
			DirectX::XMFLOAT3		m_upDir;
			DirectX::XMFLOAT2		m_yawPitch;
			bool					m_freeCam;

		public:
			ENGINE_API BaseCamera();
			ENGINE_API virtual ~BaseCamera();

			ENGINE_API void ResetCamera();
			ENGINE_API void SetProjection(
				_In_ float32 fovAngleY,
				_In_ float32 aspectRatio,
				_In_ float32 nearZ,
				_In_ float32 farZ
			);
			ENGINE_API void SetLookAt(
				_In_ float32 eyeX,
				_In_ float32 eyeY,
				_In_ float32 eyeZ,
				_In_ float32 lookX,
				_In_ float32 lookY,
				_In_ float32 lookZ,
				_In_ float32 upX,
				_In_ float32 upY,
				_In_ float32 upZ
			);
			ENGINE_API void SetPosition(
				_In_ float32 posX,
				_In_ float32 posY,
				_In_ float32 posZ
			);
			ENGINE_API void SetView(
				_In_ float32 viewX,
				_In_ float32 viewY
			);
			ENGINE_API void SetMinMax(
				_In_ float32 min,
				_In_ float32 max
			);
			ENGINE_API void Move(
				_In_ float32 value
			);
			ENGINE_API void Shift(
				_In_ float32 value
			);
			ENGINE_API void Rise(
				_In_ float32 value
			);
			ENGINE_API void Rotate(
				_In_ float32 horizontal,
				_In_ float32 vertical
			);

			void SetEyePosition(
				_In_ const DirectX::XMFLOAT3 &eyePos
			)
			{ 
				m_eyePos = eyePos;
			}

			void SetLookDirection(
				_In_ const DirectX::XMFLOAT3 &lookDir
			)
			{
				m_lookDir = lookDir;
			}

			void SetSetUpDirection(
				_In_ const DirectX::XMFLOAT3 &upDir
			)
			{
				m_upDir = upDir;
			}

			DirectX::XMFLOAT3 GetEye() { return m_eyePos; }
			DirectX::XMFLOAT3 GetLook() { return m_lookDir; }
			DirectX::XMFLOAT3 GetUp() { return m_upDir; }

			virtual DirectX::XMFLOAT4X4 ViewMatrix() = 0;
			virtual DirectX::XMFLOAT4X4 ProjectionMatrix() = 0;
		};

		class BaseCameraFPS : public BaseCamera
		{
		public:
			ENGINE_API ~BaseCameraFPS() override;

			ENGINE_API DirectX::XMFLOAT4X4 ViewMatrix() override;
			ENGINE_API DirectX::XMFLOAT4X4 ProjectionMatrix() override;

		public:
			ENGINE_API static BaseCameraFPS* CreateCamera();

		private:
			BaseCameraFPS();

			void Update() override;
		};
	}
}