﻿#pragma once

#include "targetver.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>

#include <wrl.h>
#include <wrl/client.h>
#include <ppltasks.h>
#include <dxgi1_4.h>
#include <d3d11_3.h>
#include <d2d1_3.h>
#include <d2d1effects_2.h>
#include <dwrite_3.h>
#include <wincodec.h>
#include <DirectXColors.h>
#include <DirectXMath.h>
#include <windows.ui.xaml.media.dxinterop.h>
#include <memory>
#include <agile.h>
#include <concrt.h>
#include <collection.h>
#include <d3d11_2.h>
#include <queue>
#include <map>
