#define LIGHT_MAX 7

Texture2D tex : register (t0);
SamplerState samp : register (s0);

struct PixelShaderInput
{
	float4	pos : SV_POSITION;
	float3	color : COLOR0;
	float2	uv : TEXCOORD0;
	float4	norm_pos : TEXCOORD1;
	float4	norm_dir : TEXCOORD2;
};

struct LightHeader
{
	uint4	info_meta;
	float4	light_meta;
};

struct LightSource
{
	float4	light_meta;
	float4	light_pos;
	float4	light_dir;
	float4	light_col;
};

cbuffer LightsConstantBuffer : register(b0)
{
	LightHeader	header;
	LightSource	lightArray[LIGHT_MAX];
	float4		align_1;
	float4		align_2;
	float4		align_3;
};

float4 main(PixelShaderInput input) : SV_TARGET
{
	float3 diffValue = float3(0, 0, 0);
	float3 normalVec = normalize(input.norm_dir.xyz);

	for (uint i = 0; i < header.info_meta.x && i < LIGHT_MAX; ++i)
	{
		float3 rayDir = normalize(
			lightArray[i].light_pos.xyz - input.norm_pos.xyz
		);

		float diffTmp = max(dot(rayDir, normalVec), 0.0f);
		diffValue += (lightArray[i].light_col.xyz * diffTmp);
	}
	float3 texelColor = 
		tex.Sample(samp, input.uv) *
		clamp(diffValue, header.light_meta.x, 1.0f);

	return float4(texelColor, 1.0f);
}
