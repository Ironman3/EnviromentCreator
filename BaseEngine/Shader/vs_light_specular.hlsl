struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 norm : NORMAL;
	float2 tex : TEXCOORD0;
	float4 color : COLOR0;
};

struct PixelShaderInput
{
	float4	pos : SV_POSITION;
	float3	color : COLOR0;
	float2	uv : TEXCOORD0;
	float4	norm_pos : TEXCOORD1;
	float4	norm_dir : TEXCOORD2;
	float3	cam_pos : TEXCOORD3;
	float3	cam_dir : TEXCOORD4;
};

cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix	model;
	matrix	view;
	matrix	projection;
	float4	eye;
	float4	look;
	float4	align_1;
	float4	align_2;
};

PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;

	output.norm_pos = mul(
		float4(input.pos, 1.0f),
		model
	);
	output.pos = mul(
		mul(output.norm_pos, view),
		projection
	);
	output.norm_dir = mul(
		float4(input.pos + input.norm, 1.0f),
		model
	) - output.norm_pos;
	output.color = input.color;
	output.uv = input.tex;
	output.cam_pos = eye.xyz;
	output.cam_dir = look.xyz;

	return output;
}
