#define LIGHT_MAX 7

Texture2D tex : register (t0);
SamplerState samp : register (s0);

struct PixelShaderInput
{
	float4	pos : SV_POSITION;
	float3	color : COLOR0;
	float2	uv : TEXCOORD0;
	float4	norm_pos : TEXCOORD1;
	float4	norm_dir : TEXCOORD2;
	float3	cam_pos : TEXCOORD3;
	float3	cam_dir : TEXCOORD4;
};

struct LightHeader
{
	uint4	info_meta;
	float4	light_meta;
};

struct LightSource
{
	float4	light_meta;
	float4	light_pos;
	float4	light_dir;
	float4	light_col;
};

cbuffer LightsConstantBuffer : register(b0)
{
	LightHeader	header;
	LightSource	lightArray[LIGHT_MAX];
	float4		align_1;
	float4		align_2;
};

float4 main(PixelShaderInput input) : SV_TARGET
{
	float3 diffValue = float3(0, 0, 0);
	float3 specValue = float3(0, 0, 0);

	float3 normalVec = normalize(input.norm_dir.xyz);
	float3 cameraVec = normalize(input.cam_pos - input.norm_pos.xyz);

	for (uint i = 0; i < header.info_meta.x && i < LIGHT_MAX; ++i)
	{
		float3 rayDir = normalize(
			lightArray[i].light_pos.xyz - input.norm_pos.xyz
		);

		float diffTmp = max(dot(rayDir, normalVec), 0.0f);
		diffValue += (lightArray[i].light_col.xyz * diffTmp);

		float specTmp = max(dot(normalVec, (cameraVec + rayDir) * 0.5f), 0.0f);
		specValue += pow(specTmp, 1.5);
	}
	float3 texelColor = 
		tex.Sample(samp, input.uv) *
		clamp(diffValue + specValue, header.light_meta.x, 1.0f);

	return float4(texelColor, 1.0f);
}
