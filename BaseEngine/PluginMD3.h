#pragma once

#include "Plugin.h"

namespace Engine
{
	namespace Plugin
	{
		namespace Model
		{
#define Q_PI 3.14159265358979323846

			struct Vector3
			{
			public:
				float x, y, z;
			};

			struct MD3_Header
			{
			public:
				int		ident;
				int		version;
				char	name[64];
				int		flags;
				int		num_frames;
				int		num_tags;
				int		num_surfaces;
				int		num_skins;
				int		ofs_frames;
				int		ofs_tags;
				int		ofs_surfaces;
				int		ofs_eof;
			};

			struct MD3_Frame
			{
			public:
				Vector3	minBounds;
				Vector3 maxBounds;
				Vector3 localOrigin;
				float	radius;
				char	name[16];
			};

			struct MD3_Tag
			{
			public:
				char	name[64];
				Vector3	origin;
				Vector3 axis[3];
			};

			struct MD3_Shader
			{
			public:
				char	name[64];
				int		shaderIndex;
			};

			struct MD3_Triangle
			{
			public:
				int indexes[3];
			};

			struct MD3_Texcoord
			{
			public:
				float st[2];
			};

			struct MD3_Vertex
			{
			public:
				short coord[3];
				short normal;
			};

			struct MD3_Surface
			{
			public:
				struct Header
				{
				public:
					int		ident;
					char	name[64];
					int		flags;
					int		num_frames;
					int		num_shaders;
					int		num_vertices;
					int		num_triangles;
					int		ofs_triangles;
					int		ofs_shaders;
					int		ofs_st;
					int		ofs_xyzNormal;
					int		ofs_end;
				};
			public:
				MD3_Surface::Header	header;
				MD3_Shader*			p_shaderArray;
				MD3_Triangle*		p_triangleArray;
				MD3_Texcoord*		p_texcoordArray;
				MD3_Vertex*			p_vertexArray;
			};

			struct MD3_File
			{
			public:
				MD3_Header		header;
				MD3_Frame*		p_frameArray;
				MD3_Tag*		p_tagArray;
				MD3_Surface*	p_surfaceArray;
			};

			class LoaderMD3 : public IPluginModel
			{
			public:
				ENGINE_API void LoadFromFile(
					_In_ std::wstring fileName,
					_Out_ Stream::ModelMeshStream **pp_modelMeshStream
				);
				ENGINE_API void BuildFromStream(
					_In_ FileData ^fileData,
					_Out_ Stream::ModelMeshStream **pp_modelMeshStream
				);

			private:
				void ReadBlock_MD3();
				void ReadBlock_Surface(
					_Out_ MD3_Surface &surface
				);

				MD3_Header ReadHeader();
				MD3_Surface::Header ReadSurfaceHeader();

				MD3_Frame* ReadFrameArray(
					_In_ uint32 numOf
				);
				MD3_Tag* ReadTagArray(
					_In_ uint32 numOf
				);
				MD3_Surface* ReadSurfaceArray(
					_In_ uint32 numOf
				);
				MD3_Shader* ReadShaderArray(
					_In_ uint32 numOf
				);
				MD3_Triangle* ReadTriangleArray(
					_In_ uint32 numOf
				);
				MD3_Texcoord* ReadTexCoordArray(
					_In_ uint32 numOf
				);
				MD3_Vertex* ReadVertexArray(
					_In_ uint32 numOf
				);

			private:
				void ReadStream(
					_Inout_ byte *p_stream,
					_In_ uint32 length
				);
				void Skip(
					_In_ uint32 length
				);
				void Seek(
					_In_ uint32 offset
				);

			private:
				Stream::ModelMeshStream* CreateModelMeshStream();
				
				void InitMeshStreamArray(
					_Out_ Stream::ModelMeshStream *p_modelMeshStream
				);
				void InitVertexArray(
					_In_ MD3_Surface &surface,
					_Out_  Stream::ModelMeshStream::SubMeshStream *p_subMeshStream
				);
				void InitIndexArray(
					_In_ MD3_Surface &surface,
					_Out_  Stream::ModelMeshStream::SubMeshStream *p_subMeshStream
				);
				void InitAnimKeyStreamArray(
					_Out_ Stream::ModelMeshStream *p_modelMeshStream
				);

			private:
				std::unique_ptr<MD3_File>	m_md3;

				uint32						m_offset;
				FileData^					m_dataStreamPtr;
			};
		}
	}
}
