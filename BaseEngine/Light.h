#pragma once

#include "Common.h"

namespace Engine
{
	namespace Light
	{
		enum LIGHT_TYPES
		{
			Sun = 0,
			Point = 1,
			Direction = 2,
		};

		class BaseLight
		{
		public:
			BaseLight(
				_In_ LIGHT_TYPES type,
				_In_ float intensity
			) : m_position(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
				m_direction(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
				m_color(DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)) {}

			void SetPosition(
				_In_ DirectX::XMFLOAT3 &position
			)
			{
				m_position = position;
			}

			void SetDirection(
				_In_ DirectX::XMFLOAT3 &direction
			)
			{
				m_direction = direction;
			}

			void SetColor(
				_In_ DirectX::XMFLOAT4 &color
			)
			{
				m_color = color;
			}

			DirectX::XMFLOAT3 GetPosition() { return m_position; }
			DirectX::XMFLOAT3 GetDirection() { return m_direction; }
			DirectX::XMFLOAT3 GetColor_RGB() { return DirectX::XMFLOAT3(m_color.x, m_color.y, m_color.z); }
			DirectX::XMFLOAT4 GetColor_RGBA() { return m_color; }

		private:
			DirectX::XMFLOAT3 m_position;
			DirectX::XMFLOAT3 m_direction;
			DirectX::XMFLOAT4 m_color;
		};
	}
}