#pragma once
#include "Plugin.h"

namespace Engine
{
	namespace Plugin
	{
		namespace Bitmap
		{
			class LoaderBMP : public IPluginBitmap
			{
			public:
				ENGINE_API void LoadFromFile(
					_In_ std::wstring fullPath,
					_Out_ Stream::BitmapStream **pp_bitmapStream
				);
				ENGINE_API void BuildFromStream(
					_In_ FileData ^fileData,
					_Out_ Stream::BitmapStream **pp_bitmapStream
				);
			};
		}
	}
}