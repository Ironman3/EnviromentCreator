#include "pch.h"
#include "PluginMD3.h"

using namespace Engine;
using namespace Engine::Plugin;
using namespace Engine::Plugin::Model;
using namespace Engine::Stream;
using namespace Engine::IO::File;

using namespace std;

void LoaderMD3::LoadFromFile(
	_In_ wstring fileName,
	_Out_ ModelMeshStream **pp_modelMeshStream
)
{
	BuildFromStream(
		ReadData(fileName),
		pp_modelMeshStream
	);
}

void LoaderMD3::BuildFromStream(
	_In_ FileData ^fileData,
	_Out_ ModelMeshStream **pp_modelMeshStream
)
{
	m_offset = 0;
	m_dataStreamPtr = fileData;
	m_md3 = unique_ptr<MD3_File>(
		new MD3_File()
		);

	ReadBlock_MD3();
	*pp_modelMeshStream = CreateModelMeshStream();
}

void LoaderMD3::ReadBlock_MD3()
{
	m_md3->header = ReadHeader();
	m_md3->p_frameArray = ReadFrameArray(m_md3->header.num_frames);
	m_md3->p_tagArray = ReadTagArray(m_md3->header.num_tags * m_md3->header.num_frames);
	m_md3->p_surfaceArray = ReadSurfaceArray(m_md3->header.num_surfaces);
}

MD3_Header LoaderMD3::ReadHeader()
{
	MD3_Header header;
	ReadStream(
		reinterpret_cast<byte*>(&header),
		sizeof(MD3_Header)
	);

	return header;
}

MD3_Frame* LoaderMD3::ReadFrameArray(
	_In_ uint32 numOf
)
{
	Seek(m_md3->header.ofs_frames);
	MD3_Frame *frameArrayPtr = new MD3_Frame[numOf];
	ReadStream(
		reinterpret_cast<byte*>(frameArrayPtr),
		sizeof(MD3_Frame) * numOf
	);

	return frameArrayPtr;
}

MD3_Tag* LoaderMD3::ReadTagArray(
	_In_ uint32 numOf
)
{
	Seek(m_md3->header.ofs_tags);
	MD3_Tag *tagArrayPtr = new MD3_Tag[numOf];
	ReadStream(
		reinterpret_cast<byte*>(tagArrayPtr),
		sizeof(MD3_Tag) * numOf
	);

	return tagArrayPtr;
}

MD3_Surface* LoaderMD3::ReadSurfaceArray(
	_In_ uint32 numOf
)
{
	Seek(m_md3->header.ofs_surfaces);
	MD3_Surface  *surfaceArrayPtr = new MD3_Surface[m_md3->header.num_surfaces];
	for (int i = 0; i < m_md3->header.num_surfaces; ++i)
		ReadBlock_Surface(surfaceArrayPtr[i]);

	return surfaceArrayPtr;
}

void LoaderMD3::ReadBlock_Surface(
	_Out_ MD3_Surface &surface
)
{
	surface.header = ReadSurfaceHeader();

	Seek(m_md3->header.ofs_surfaces + surface.header.ofs_shaders);
	surface.p_shaderArray = ReadShaderArray(surface.header.num_shaders);

	Seek(m_md3->header.ofs_surfaces + surface.header.ofs_triangles);
	surface.p_triangleArray = ReadTriangleArray(surface.header.num_triangles);

	Seek(m_md3->header.ofs_surfaces + surface.header.ofs_st);
	surface.p_texcoordArray = ReadTexCoordArray(surface.header.num_vertices);

	Seek(m_md3->header.ofs_surfaces + surface.header.ofs_xyzNormal);
	surface.p_vertexArray = ReadVertexArray(surface.header.num_frames * surface.header.num_vertices);

	Seek(m_md3->header.ofs_surfaces + surface.header.ofs_end);
	m_md3->header.ofs_surfaces += surface.header.ofs_end;
}

MD3_Surface::Header LoaderMD3::ReadSurfaceHeader()
{
	MD3_Surface::Header header;
	ReadStream(
		reinterpret_cast<byte*>(&header),
		sizeof(MD3_Surface::Header)
	);

	return header;
}

MD3_Shader* LoaderMD3::ReadShaderArray(
	_In_ uint32 numOf
)
{
	MD3_Shader *shaderArrayPtr = new MD3_Shader[numOf];
	ReadStream(
		reinterpret_cast<byte*>(shaderArrayPtr),
		sizeof(MD3_Shader) * numOf
	);

	return shaderArrayPtr;
}

MD3_Triangle* LoaderMD3::ReadTriangleArray(
	_In_ uint32 numOf
)
{
	MD3_Triangle *triangleArrayPtr = new MD3_Triangle[numOf];
	ReadStream(
		reinterpret_cast<byte*>(triangleArrayPtr),
		sizeof(MD3_Triangle) * numOf
	);

	return triangleArrayPtr;
}

MD3_Texcoord* LoaderMD3::ReadTexCoordArray(
	_In_ uint32 numOf
)
{
	MD3_Texcoord *texCoordArrayPtr = new MD3_Texcoord[numOf];
	ReadStream(
		reinterpret_cast<byte*>(texCoordArrayPtr),
		sizeof(MD3_Texcoord) * numOf
	);

	return texCoordArrayPtr;
}

MD3_Vertex* LoaderMD3::ReadVertexArray(
	_In_ uint32 numOf
)
{
	MD3_Vertex *vertexArrayPtr = new MD3_Vertex[numOf];
	ReadStream(
		reinterpret_cast<byte*>(vertexArrayPtr),
		sizeof(MD3_Vertex) * numOf
	);

	return vertexArrayPtr;
}

void LoaderMD3::ReadStream(
	_Inout_ byte *p_stream,
	_In_ uint32 length
)
{
	if (!p_stream || m_offset + length > m_dataStreamPtr->Length)
		ThrowIfFailed(E_FAIL);

	try
	{
		memcpy(
			p_stream,
			m_dataStreamPtr->Data + m_offset,
			length
		);
		Skip(length);
	}
	catch (const std::exception&)
	{
		ThrowIfFailed(E_FAIL);
	}
}

void LoaderMD3::Skip(
	_In_ uint32 length
)
{
	Seek(m_offset + length);
}

void LoaderMD3::Seek(
	_In_ uint32 offset
)
{
	if (offset > m_dataStreamPtr->Length)
		ThrowIfFailed(E_FAIL);

	m_offset = offset;
}

#pragma Convert

ModelMeshStream* LoaderMD3::CreateModelMeshStream()
{
	ModelMeshStream *modelMeshStreamPtr = new ModelMeshStream();

	InitMeshStreamArray(modelMeshStreamPtr);
	InitAnimKeyStreamArray(modelMeshStreamPtr);

	return modelMeshStreamPtr;
}

void LoaderMD3::InitMeshStreamArray(
	_Out_ ModelMeshStream *p_modelMeshStream
)
{
	p_modelMeshStream->num_mesh = m_md3->header.num_surfaces;
	p_modelMeshStream->p_array_subMeshStream = new ModelMeshStream::SubMeshStream[p_modelMeshStream->num_mesh];

	auto itSurfaceBegin = m_md3->p_surfaceArray;
	auto itSurfaceEnd = m_md3->p_surfaceArray + m_md3->header.num_surfaces;
	for (auto itSurface = itSurfaceBegin; itSurface < itSurfaceEnd; ++itSurface)
	{
		auto subMeshPtr = &p_modelMeshStream->p_array_subMeshStream[itSurface - itSurfaceBegin];

		InitIndexArray(*itSurface, subMeshPtr);
		InitVertexArray(*itSurface, subMeshPtr);
	}
}

void LoaderMD3::InitVertexArray(
	_In_ MD3_Surface &surface,
	_Out_  ModelMeshStream::SubMeshStream *p_subMeshStream
)
{
	p_subMeshStream->num_vertex = surface.header.num_vertices;
	p_subMeshStream->num_frame = surface.header.num_frames;
	p_subMeshStream->p_array_vertex = new Vertex3D[p_subMeshStream->num_vertex * p_subMeshStream->num_frame];

	for (auto i = 0; i < p_subMeshStream->num_vertex * p_subMeshStream->num_frame; ++i)
	{
		p_subMeshStream->p_array_vertex[i].pos.x = (float)surface.p_vertexArray[i].coord[0] * (1.0 / 64);
		p_subMeshStream->p_array_vertex[i].pos.y = (float)surface.p_vertexArray[i].coord[2] * (1.0 / 64);
		p_subMeshStream->p_array_vertex[i].pos.z = (float)surface.p_vertexArray[i].coord[1] * (1.0 / 64);

		float lat;
		float lng;
		lat = (surface.p_vertexArray[i].normal >> 8) & 0xff;
		lng = (surface.p_vertexArray[i].normal & 0xff);
		lat *= Q_PI / 128;
		lng *= Q_PI / 128;

		p_subMeshStream->p_array_vertex[i].norm.x = cos(lat) * sin(lng);
		p_subMeshStream->p_array_vertex[i].norm.z = sin(lat) * sin(lng);
		p_subMeshStream->p_array_vertex[i].norm.y = cos(lng);
	}
}

void LoaderMD3::InitIndexArray(
	_In_ MD3_Surface &surface,
	_Out_  ModelMeshStream::SubMeshStream *p_subMeshStream
)
{
	p_subMeshStream->num_index = surface.header.num_triangles * 3;
	p_subMeshStream->p_array_index = new VertexIndex[p_subMeshStream->num_index];

	for (auto i = 0; i < surface.header.num_triangles; ++i)
	{
		p_subMeshStream->p_array_index[i * 3] = static_cast<uint32>(surface.p_triangleArray[i].indexes[0]);
		p_subMeshStream->p_array_index[(i * 3) + 1] = static_cast<uint32>(surface.p_triangleArray[i].indexes[1]);
		p_subMeshStream->p_array_index[(i * 3) + 2] = static_cast<uint32>(surface.p_triangleArray[i].indexes[2]);
	}
}

void LoaderMD3::InitAnimKeyStreamArray(
	_Out_ ModelMeshStream *p_modelMeshStream
)
{
	p_modelMeshStream->num_anim = m_md3->header.num_frames;
	p_modelMeshStream->p_array_animKeyStream = new ModelMeshStream::AnimKeyStream[p_modelMeshStream->num_anim];

	auto itFrameBegin = m_md3->p_tagArray;
	auto itFrameEnd = m_md3->p_tagArray + m_md3->header.num_tags;
	for (auto itFrame = itFrameBegin; itFrame < itFrameEnd; ++itFrame)
	{
		auto &frameRef = p_modelMeshStream->p_array_animKeyStream[itFrame - itFrameBegin];
		frameRef.keyName = itFrame->name;
	}
}


