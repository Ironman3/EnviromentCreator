#pragma once

#include <mutex>
#include <shared_mutex>

#include "Common.h"

namespace Engine
{
	namespace Devices
	{
		template <class T>
		class DeviceHandler
		{
		public:
			DeviceHandler() {}

			void Append(
				_In_ std::wstring uniqueName,
				_In_ T *p_device
			)
			{
				std::unique_lock<std::shared_mutex> lock(m_locker);
				m_deviceList[uniqueName] = std::unique_ptr<T>(p_device);
			}

			void Delete(
				_In_ std::wstring uniqueName
			)
			{
				std::unique_lock<std::shared_mutex> lock(m_locker);
				m_deviceList.erase(uniqueName);
			}

			size_t GetDeviceNumber()
			{
				std::shared_lock<std::shared_mutex> lock(m_locker);
				return m_deviceList.size();
			}

			bool DeviceIsExsist(
				_In_ std::wstring uniqueName
			)
			{
				std::shared_lock<std::shared_mutex> lock(m_locker);
				return m_deviceList.find(uniqueName) != m_deviceList.end();
			}

			T* GetDevice(
				_In_ std::wstring uniqueName
			)
			{
				std::shared_lock<std::shared_mutex> lock(m_locker);
				auto result = m_deviceList.find(uniqueName);
				return result != m_deviceList.end() ? result->second.get() : nullptr;
			}

			void QueryList(
				_In_ std::vector<std::wstring> &query
			)
			{
				std::shared_lock<std::shared_mutex> lock(m_locker);
				for (auto &pair : m_deviceList)
					query.push_back(pair.first);
			}

			void QueryList(
				_In_ std::vector<T*> &query
			)
			{
				std::shared_lock<std::shared_mutex> lock(m_locker);
				for (auto &pair : m_deviceList)
					query.push_back(pair.second.get());
			}
		public:
			void SetCursor(
				_In_ std::wstring cursor
			)
			{
				m_cursor = cursor;
			}

			std::wstring GetCursor()
			{
				return m_cursor;
			}
		private:
			std::map<std::wstring, std::unique_ptr<T>>	m_deviceList;
			mutable std::shared_mutex					m_locker;
			std::wstring								m_cursor;
		};
	}
}