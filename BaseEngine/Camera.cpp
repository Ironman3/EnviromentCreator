#include "pch.h"
#include "Camera.h"

using namespace Engine;
using namespace Engine::Camera;

using namespace DirectX;

BaseCamera::BaseCamera()
{
	ResetCamera();
}

BaseCamera::~BaseCamera()
{
}

void BaseCamera::ResetCamera()
{
	XMStoreFloat4x4(&m_projMat, XMMatrixIdentity());
	XMStoreFloat4x4(&m_viewMat, XMMatrixIdentity());
	XMStoreFloat4(&m_worldQuat, XMQuaternionIdentity());
	XMStoreFloat4(&m_frameQuat, XMQuaternionIdentity());
	m_eyePos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_lookDir = XMFLOAT3(0.0f, 0.0f, 1.0f);
	m_upDir = XMFLOAT3(0.0f, 1.0f, 0.0f);
	m_yawPitch = XMFLOAT2(0.0f, 0.0f);
	m_freeCam = false;
}

void BaseCamera::SetProjection(
	_In_ float32 fovAngleY,
	_In_ float32 aspectRatio,
	_In_ float32 nearZ,
	_In_ float32 farZ
)
{
	XMStoreFloat4x4(
		&m_projMat,
		XMMatrixPerspectiveFovLH(
			fovAngleY,
			aspectRatio,
			nearZ,
			farZ
		)
	);
}

void BaseCamera::SetLookAt(
	_In_ float32 eyeX,
	_In_ float32 eyeY,
	_In_ float32 eyeZ,
	_In_ float32 lookX,
	_In_ float32 lookY,
	_In_ float32 lookZ,
	_In_ float32 upX,
	_In_ float32 upY,
	_In_ float32 upZ
)
{
	XMStoreFloat4x4(
		&m_viewMat,
		XMMatrixLookAtLH(
			XMVectorSet(eyeX, eyeY, eyeZ, 0.0f),
			XMVectorSet(lookX, lookY, lookZ, 0.0f),
			XMVectorSet(upX, upY, upZ, 0.0f)
		)
	);
}

void BaseCamera::SetPosition(
	_In_ float32 posX,
	_In_ float32 posY,
	_In_ float32 posZ
)
{
	m_eyePos = XMFLOAT3(posX, posY, posZ);
}

void BaseCamera::SetView(
	_In_ float32 vieX,
	_In_ float32 vieY
)
{
	m_yawPitch = XMFLOAT2(vieX, vieY);
}

void BaseCamera::SetMinMax(
	_In_ float32 min,
	_In_ float32 max
)
{
	register float viewY = m_yawPitch.y;

	if (viewY < min) viewY = min;
	if (viewY > max) viewY = max;
}

void BaseCamera::Move(
	_In_ float32 value
)
{
	if (m_freeCam)
	{
		XMStoreFloat3(
			&m_eyePos,
			XMLoadFloat3(&m_eyePos) + XMLoadFloat3(&m_lookDir)
		);
	}
	else
	{
		XMFLOAT2 moveDir = XMFLOAT2(m_lookDir.x, m_lookDir.z);
		XMStoreFloat2(
			&moveDir,
			XMVector2Normalize(XMLoadFloat2(&moveDir)) * value
		);

		m_eyePos.x += moveDir.x;
		m_eyePos.z += moveDir.y;
	}
}

void BaseCamera::Shift(
	_In_ float32 value
)
{
	throw Platform::NotImplementedException::CreateException(0);
}

void BaseCamera::Rise(
	_In_ float32 value
)
{
	throw Platform::NotImplementedException::CreateException(0);
}

void BaseCamera::Rotate(
	_In_ float32 horizontal,
	_In_ float32 vertical
)
{
	m_yawPitch.x += horizontal;
	m_yawPitch.y += vertical;

	if (m_yawPitch.x > XM_2PI) m_yawPitch.x -= XM_2PI;
	if (m_yawPitch.x < 0.0f) m_yawPitch.x += XM_2PI;

	if (m_yawPitch.y > XM_2PI) m_yawPitch.y -= XM_2PI;
	if (m_yawPitch.y < 0.0f) m_yawPitch.y += XM_2PI;
}

//////////////////////////////////////////

BaseCameraFPS::BaseCameraFPS()
{
}

BaseCameraFPS::~BaseCameraFPS()
{
}

BaseCameraFPS* BaseCameraFPS::CreateCamera()
{
	return new BaseCameraFPS();
}

XMFLOAT4X4 BaseCameraFPS::ViewMatrix()
{
	Update();

	XMFLOAT4X4 viewResult;
	XMStoreFloat4x4(
		&viewResult,
		XMLoadFloat4x4(&m_viewMat)
	);

	return viewResult;
}

XMFLOAT4X4 BaseCameraFPS::ProjectionMatrix()
{
	Update();

	XMFLOAT4X4 projResult;
	XMStoreFloat4x4(
		&projResult,
		XMLoadFloat4x4(&m_projMat)
	);

	return projResult;
}

void BaseCameraFPS::Update()
{
	XMStoreFloat4(
		&m_frameQuat,
		XMQuaternionRotationRollPitchYaw(m_yawPitch.x, m_yawPitch.y, 0.0f)
	);

	if (m_freeCam)
	{
		XMStoreFloat4(&m_worldQuat, XMLoadFloat4(&m_frameQuat) * XMLoadFloat4(&m_worldQuat));
		XMStoreFloat4(&m_frameQuat, XMQuaternionIdentity());
	}
	else
	{
		m_worldQuat = m_frameQuat;
	}

	XMFLOAT4X4 orientation;
	XMStoreFloat4x4(&orientation,
		XMMatrixAffineTransformation(
			XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f),
			XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
			XMLoadFloat4(&m_worldQuat),
			XMLoadFloat3(&m_eyePos)
		)
	);

	XMStoreFloat4x4(
		&m_viewMat,
		XMMatrixInverse(nullptr, XMLoadFloat4x4(&orientation))
	);

	XMFLOAT4X4 transform;
	XMStoreFloat4x4(&transform,
		XMMatrixAffineTransformation(
			XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f),
			XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
			XMLoadFloat4(&m_worldQuat),
			XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)
		)
	);

	XMStoreFloat3(
		&m_lookDir,
		XMVector4Normalize(
			XMVector4Transform(XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), XMLoadFloat4x4(&transform))
		)
	);

	XMStoreFloat3(
		&m_upDir,
		XMVector4Normalize(
			XMVector4Transform(XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f), XMLoadFloat4x4(&transform))
		)
	);
}
