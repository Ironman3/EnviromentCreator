#include "pch.h"
#include "PluginBMP.h"

using namespace Engine;
using namespace Engine::Plugin;
using namespace Engine::Plugin::Bitmap;
using namespace Engine::Stream;
using namespace Engine::IO::File;

using namespace std;

void LoaderBMP::LoadFromFile(
	_In_ std::wstring fullPath,
	_Out_ BitmapStream **pp_bitmapStream
)
{
	BuildFromStream(
		ReadData(fullPath),
		pp_bitmapStream
	);
}

void LoaderBMP::BuildFromStream(
	_In_ FileData ^fileData,
	_Out_ BitmapStream **pp_bitmapStream
)
{
	BitmapStream *bitmapStreamPtr = new BitmapStream();
	int32 startBit;
	int16 bitPerPixel;

	memcpy(&startBit, &fileData->Data[10], sizeof(int32));
	memcpy(&bitmapStreamPtr->width, &fileData->Data[18], sizeof(int32));
	memcpy(&bitmapStreamPtr->height, &fileData->Data[22], sizeof(int32));
	memcpy(&bitPerPixel, &fileData->Data[28], sizeof(int16));

	register auto dataPtr = &fileData->Data[startBit];
	uint32 padding = bitmapStreamPtr->width % 4;
	uint32 rowPack = ((bitPerPixel * bitmapStreamPtr->width + 31) / 32) * 4;
	uint32 rowSizeInByte = rowPack - padding;
	uint32 pixelArraySize = bitmapStreamPtr->width * bitmapStreamPtr->height * 4;

	bitmapStreamPtr->p_array_byte = new byte[pixelArraySize];

	register byte *arrayPtr = bitmapStreamPtr->p_array_byte;

	uint32 i, i2;
	for (i = 0; i < bitmapStreamPtr->height; ++i)
	{
		for (i2 = 0; i2 < rowSizeInByte; i2 += 3)
		{
			*arrayPtr++ = dataPtr[2];
			*arrayPtr++ = dataPtr[1];
			*arrayPtr++ = dataPtr[0];
			*arrayPtr++ = 255;

			dataPtr += 3;
		}
		dataPtr += padding;
	}

	*pp_bitmapStream = bitmapStreamPtr;
}


