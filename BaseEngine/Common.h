#pragma once

#include <pch.h>

namespace Engine
{
#ifdef _DLL  
#define ENGINE_API __declspec(dllexport)  
#else  
#define ENGINE_API   
#endif  

#define LIGHT_MAX 7

	typedef Platform::Array<byte> FileData;

	static void ThrowIfFailed(
		_In_ HRESULT hResult
	)
	{
		if (FAILED(hResult))
		{
			auto ex = hResult;
			return;
		}
			//throw Platform::Exception::CreateException(hResult);
	}

	//template <class T>
	//class ReservStack
	//{
	//public:
	//	Stack() : m_size(100), m_pointer(0), m_start(0)
	//	{
	//		m_arrayPtr = new T[m_size];
	//	}

	//	Stack(
	//		_In_ uint32 size
	//	) : m_size(size), m_pointer(0), m_start(0)
	//	{
	//		m_arrayPtr = new T[m_size];
	//	}

	//	void PushBack(
	//		_In_ T *p_element
	//	)
	//	{
	//		if (m_pointer < m_size)
	//			m_arrayPtr[m_pointer++] = p_element;
	//	}

	//	T* PopFirst()
	//	{
	//		if(m_start > 0 && m_start < )

	//	}

	//private:
	//	uint32	m_size;
	//	uint32	m_pointer;
	//	uint32	m_start;
	//	T*		m_arrayPtr;
	//};

	struct Ray
	{
	public:
		DirectX::XMFLOAT3 rayPos;
		DirectX::XMFLOAT3 rayDir;
	};

	namespace IO
	{
		namespace File
		{
			static inline FileData^ ReadData(
				_In_ const std::wstring &fileName
			)
			{
				CREATEFILE2_EXTENDED_PARAMETERS extendedParams = { 0 };
				extendedParams.dwSize = sizeof(CREATEFILE2_EXTENDED_PARAMETERS);
				extendedParams.dwFileAttributes = FILE_ATTRIBUTE_NORMAL;
				extendedParams.dwFileFlags = FILE_FLAG_SEQUENTIAL_SCAN;
				extendedParams.dwSecurityQosFlags = SECURITY_ANONYMOUS;
				extendedParams.lpSecurityAttributes = nullptr;
				extendedParams.hTemplateFile = nullptr;

				Microsoft::WRL::Wrappers::FileHandle file(
					CreateFile2(
						fileName.c_str(),
						GENERIC_READ,
						FILE_SHARE_READ,
						OPEN_EXISTING,
						&extendedParams
					)
				);

				if (file.Get() == INVALID_HANDLE_VALUE)
				{
					throw ref new Platform::FailureException();
				}

				FILE_STANDARD_INFO fileInfo = { 0 };

				if (!GetFileInformationByHandleEx(
					file.Get(),
					FileStandardInfo,
					&fileInfo,
					sizeof(fileInfo)
				))
				{
					throw ref new Platform::FailureException();
				}

				if (fileInfo.EndOfFile.HighPart != 0)
				{
					throw ref new Platform::OutOfMemoryException();
				}

				auto fileData = ref new Platform::Array<byte>(fileInfo.EndOfFile.LowPart);

				if (!ReadFile(
					file.Get(),
					fileData->Data,
					fileData->Length,
					nullptr,
					nullptr
				))
				{
					throw ref new Platform::FailureException();
				}

				return fileData;
			}
		}
	}

	struct ShaderModelViewProj
	{
	public:
		DirectX::XMFLOAT4X4 model;
		DirectX::XMFLOAT4X4 view;
		DirectX::XMFLOAT4X4	proj;
		DirectX::XMFLOAT4	align_1;
		DirectX::XMFLOAT4	align_2;
		DirectX::XMFLOAT4	align_3;
		DirectX::XMFLOAT4	align_4;
	};

	struct ShaderLights
	{
	public:
		struct Header
		{
			uint32				info_meta[4];
			DirectX::XMFLOAT4	light_meta;
		};

		struct Source 
		{
			DirectX::XMFLOAT4	light_meta;
			DirectX::XMFLOAT4	light_pos;
			DirectX::XMFLOAT4	light_dir;
			DirectX::XMFLOAT4	light_col;
		};
	public:
		Header				lightHeader;
		Source				lightArray[LIGHT_MAX]; 
		DirectX::XMFLOAT4	align_1; 
		DirectX::XMFLOAT4	align_2; 
	};

	typedef uint32 VertexIndex;

	struct Vertex3D
	{
	public:
		Vertex3D() : pos(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
			norm(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
			tex(DirectX::XMFLOAT2(0.0f, 0.0f)),
			col(DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f)) {}
		Vertex3D(
			_In_ DirectX::XMFLOAT3 pos, 
			_In_ DirectX::XMFLOAT3 norm, 
			_In_ DirectX::XMFLOAT2 tex, 
			_In_ DirectX::XMFLOAT4 col
		) : pos(pos), norm(norm), tex(tex), col(col) {}

		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 norm;
		DirectX::XMFLOAT2 tex;
		DirectX::XMFLOAT4 col;
	};

	namespace Stream
	{
		__interface IStream
		{
		};

		struct BitmapStream : public IStream
		{
		public:
			uint32	width;
			uint32	height;
			byte*	p_array_byte;
		};

		struct ModelMeshStream : public IStream
		{
		public:
			struct SubMeshStream
			{
			public:
				uint32			num_vertex;
				uint32			num_index;
				uint32			num_frame;

				Vertex3D*		p_array_vertex;
				VertexIndex*	p_array_index;

				~SubMeshStream()
				{
					if (p_array_vertex)
					{
						delete[] p_array_vertex;
						p_array_vertex = nullptr;
					}

					if (p_array_index)
					{
						delete[] p_array_index;
						p_array_index = nullptr;
					}
				}
			};

			struct AnimKeyStream
			{
			public:
				std::string keyName;
			};
		public:
			uint32			num_mesh;
			uint32			num_anim;

			SubMeshStream*	p_array_subMeshStream;
			AnimKeyStream*	p_array_animKeyStream;

			~ModelMeshStream()
			{
				if (p_array_subMeshStream)
				{
					delete[] p_array_subMeshStream;
					p_array_subMeshStream = nullptr;
				}

				if (p_array_animKeyStream)
				{
					delete[] p_array_animKeyStream;
					p_array_animKeyStream = nullptr;
				}
			}
		};
	}

	namespace Resource
	{
		namespace Bag
		{
			struct RasterizerBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizerState;

				RasterizerBag(
					_In_ ID3D11RasterizerState *p_rasterizerState
				) : rasterizerState(p_rasterizerState) {}
			};

			struct DepthStencilBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depthStencilState;

				DepthStencilBag(
					_In_ ID3D11DepthStencilState *p_depthStencilState
				) : depthStencilState(p_depthStencilState) {}
			};

			struct SamplerBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11SamplerState>	samplerStates;
				uint32										slotId;
				uint32										samplerNum;

				SamplerBag(
					_In_ ID3D11SamplerState *p_samplerStates,
					_In_ uint32 slotId,
					_In_ uint32 samplerNum
				) : samplerStates(p_samplerStates), slotId(slotId), samplerNum(samplerNum) {}
			};

			struct BlendBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11BlendState>	blendState;
				uint32										sampleMask;
				DirectX::XMFLOAT4*							p_blendFactor;

				BlendBag(
					_In_ ID3D11BlendState *p_blendState,
					_In_ uint32 sampleMask,
					_In_ DirectX::XMFLOAT4 *p_blendFactor
				) : blendState(p_blendState), sampleMask(sampleMask), p_blendFactor(p_blendFactor) {}
			};

			struct ConstBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11Buffer>	constBuffer;
				uint32									strideSizeInByte;

				ConstBag() {}
				ConstBag(
					_In_ ID3D11Buffer *p_constBuffer,
					_In_ uint32 strideSizeInByte
				) : constBuffer(p_constBuffer), strideSizeInByte(strideSizeInByte) {}
			};

			struct VertexShaderBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
				Microsoft::WRL::ComPtr<ID3D11InputLayout>	inputLayout;
				ConstBag									constBufferArray[5];

				VertexShaderBag() : vertexShader(nullptr) {}
				VertexShaderBag(
					_In_ ID3D11VertexShader *p_vertexShader,
					_In_ ID3D11InputLayout *p_inputLayout
				) : vertexShader(p_vertexShader), inputLayout(p_inputLayout) {}
			};

			struct PixelShaderBag
			{
			public:
				Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
				ConstBag									constBufferArray[5];

				PixelShaderBag() : pixelShader(nullptr) {}
				PixelShaderBag(
					_In_ ID3D11PixelShader *p_pixelShader
				) : pixelShader(p_pixelShader) {}
			};
		}

		struct TextureObject
		{
		public:
			void InitTexture(
				_In_ ID3D11Texture2D *p_texture,
				_In_ ID3D11ShaderResourceView *p_shaderResourceView
			)
			{
				if (!p_texture || p_shaderResourceView)
					ThrowIfFailed(E_FAIL);

				m_texture = p_texture;
				m_shaderResourceView = p_shaderResourceView;
			}

			ID3D11Texture2D*			GetTexture2D() { return m_texture.Get(); }
			ID3D11ShaderResourceView*	GetShaderResourceView() { return m_shaderResourceView.Get(); }

		private:
			Microsoft::WRL::ComPtr<ID3D11Texture2D>				m_texture;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>	m_shaderResourceView;
		};

		class ModelObject
		{
		public:
			class MeshObject
			{
			public:
				MeshObject() : m_frame_index(0), m_frame_count(0) {}

				void InitVertexBuffer(
					_In_ ID3D11Buffer *p_vertexBuffer,
					_In_ uint32 num_vertex
				)
				{
					if (!p_vertexBuffer) 
						ThrowIfFailed(E_FAIL);

					m_vertexBuffer = p_vertexBuffer;
					m_num_vertex = num_vertex;
				}

				void InitIndexBuffer(
					_In_ ID3D11Buffer *p_indexBuffer,
					_In_ uint32 num_index
				)
				{
					if (!p_indexBuffer) 
						ThrowIfFailed(E_FAIL);

					m_indexBuffer = p_indexBuffer;
					m_num_index = num_index;
				}

				void InitFrames(
					_In_ uint32 frameCount
				)
				{
					m_frame_count = frameCount;
				}

				bool SetNextFrameIndex()
				{
					return SetFrameIndex(m_frame_index + 1);
				}

				bool SetFrameIndex(
					_In_ uint32 frame_index
				)
				{
					if (frame_index >= m_frame_count)
					{
						return false;
					}
					else
					{
						m_frame_index = frame_index;
						return true;
					}
				}

				uint32			GetVertexNumber() { return m_num_vertex; }
				uint32			GetIndexNumber() { return m_num_index; }
				uint32			GetFrameIndex() { return m_frame_index; }

				ID3D11Buffer*	GetIndexBuffer() { return m_indexBuffer.Get(); }
				ID3D11Buffer*	GetVertexBuffer() { return m_vertexBuffer.Get(); }

			private:
				//MeshObject() {}
				//MeshObject(const MeshObject&) {}

			private:
				Microsoft::WRL::ComPtr<ID3D11Buffer>	m_indexBuffer;
				Microsoft::WRL::ComPtr<ID3D11Buffer>	m_vertexBuffer;

				uint32									m_num_index;
				uint32									m_num_vertex;
				uint32									m_frame_index;
				uint32									m_frame_count;
			};

			class RenderObject
			{
			public:
			private:
				uint32			m_index_frame;
				uint32			m_num_texture;
				TextureObject*	m_textureObjectArray;
			};
		public:
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 rotation;
			DirectX::XMFLOAT3 scale;

			ModelObject(
				_In_ const std::wstring &parent_uniqueName,
				_In_ uint32 num_submesh
			) : 
				m_parent_uniqueName(parent_uniqueName),
				m_num_submesh(num_submesh),
				position(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
				rotation(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
				scale(DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f))
			{
				m_meshArrayPtr = new MeshObject[m_num_submesh];
			}
			
			~ModelObject()
			{
				if (m_meshArrayPtr)
				{
					delete[] m_meshArrayPtr;
					m_meshArrayPtr = nullptr;
				}
			}

			std::wstring	GetParentUniqueName() { return m_parent_uniqueName; }
			uint32			GetMeshNumber() { return m_num_submesh; }
			MeshObject*		GetMeshArray() { return m_meshArrayPtr; }

			void SetToPlay()
			{
				for (uint32 iMesh = 0; iMesh < m_num_submesh; ++iMesh)
					SetToPlay(iMesh);
			}

			void MoveToNextFrame()
			{
				for (uint32 iMesh = 0; iMesh < m_num_submesh; ++iMesh)
					MoveToNextFrame(iMesh);
			}

			void SetToPlay(
				_In_ uint32 meshIndex
			)
			{
				m_meshArrayPtr[meshIndex].SetFrameIndex(0);
			}

			void MoveToNextFrame(
				_In_ uint32 meshIndex
			)
			{
				m_meshArrayPtr[meshIndex].SetNextFrameIndex();
			}
		private:

		private:
			std::wstring	m_parent_uniqueName;
			uint32			m_num_submesh;
			uint32			m_num_clone;

			MeshObject*		m_meshArrayPtr;
		};

		class CameraObject
		{
		public:
			ShaderModelViewProj modelViewProj;

			void InitConstBuffer(
				_In_ ID3D11Buffer *p_constBuffer
			)
			{
				if (!p_constBuffer)
					ThrowIfFailed(E_FAIL);

				m_constBuffer = p_constBuffer;
			}

			ID3D11Buffer* GetConstBuffer() { return m_constBuffer.Get(); }

		private:
			Microsoft::WRL::ComPtr<ID3D11Buffer> m_constBuffer;
		};

		class LightObject
		{
		public:
			ShaderLights lights;

			void InitConstBuffer(
				_In_ ID3D11Buffer *p_constBuffer
			)
			{
				if (!p_constBuffer)
					ThrowIfFailed(E_FAIL);

				m_constBuffer = p_constBuffer;
			}

			ID3D11Buffer* GetConstBuffer() { return m_constBuffer.Get(); }

		private:
			Microsoft::WRL::ComPtr<ID3D11Buffer> m_constBuffer;
		};
	}
}
